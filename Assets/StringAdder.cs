﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class StringAdder
{
    private const char minus = '-';
    private const char Zero = '0';

    public string Minus(params string[] additions)
    {        
        string currentAddition = additions[0];
        for (int i = 1; i < additions.Length; i++)
        {
            char[] first = currentAddition.ToCharArray();
            char[] second = additions[i].ToCharArray();

            if (second.Length >= first.Length)
            {
                ChechIsSecondGreaterThenFirst(first, second);
            }

            //if (IsBothPositive(first, second))
            //{
            //    currentAddition = Plus(currentAddition, additions[i]);
            //    continue;
            //}

            //if (IsBothNegative(first, second))
            //{
            //    currentAddition = minus + Plus(currentAddition, additions[i]);
            //    continue;
            //}

            //if (IsSecondNegative(first, second))
            //{
            //    first = second;
            //    second = currentAddition.ToCharArray();
            //}

            ReverseArrays(first, second);

            int length = Mathf.Max(first.Length, second.Length);
            int rememberOne = 0;

            List<char> sumList = new List<char>();
            for (int j = 0; j < length; j++)
            {
                int firstNum = GetNumberFromChar(first, j) - rememberOne;
                int secondNum = GetNumberFromChar(second, j);

                if (firstNum < secondNum)
                {
                    firstNum += 10;
                    rememberOne = 1;
                }
                else
                {
                    rememberOne = 0;
                }
                int intSum = firstNum - secondNum;

                char num = intSum.ToString().ToCharArray().Last();
                sumList.Add(num);
            }           

            sumList.Reverse();

            RemoveAllZerosFromStart(sumList);
            currentAddition = new string(sumList.ToArray());
        }
        return currentAddition;
    }

    private static void RemoveAllZerosFromStart(List<char> sumList)
    {
        while (sumList.Count > 1 && sumList[0] == Zero)
        {
            sumList.RemoveAt(0);
        }
    }

    private void ChechIsSecondGreaterThenFirst(char[] first, char[] second)
    {
        int f = int.Parse(first[0].ToString());
        int s = int.Parse(second[0].ToString());
        if (f < s)
            Debug.LogError("second greater");

        if (f == s)
        {
            int counter = 1;
            while (f == s && counter < first.Length)
            {
                f = counter < first.Length ? int.Parse(first[counter].ToString()) : 0;
                s = counter < second.Length ? int.Parse(second[counter].ToString()) : 0;
                counter++;
                if (f < s)
                    Debug.LogError("second greater");
            }
        }
    }

    private static bool IsSecondNegative(char[] first, char[] second)
    {
        return first[0] == minus && second[0] != minus;
    }

    private static void ReverseArrays(char[] first, char[] second)
    {
        Array.Reverse(first);
        Array.Reverse(second);
    }

    private static bool IsBothNegative(char[] first, char[] second)
    {
        return first[0] == minus && second[0] == minus;
    }

    private static bool IsBothPositive(char[] first, char[] second)
    {
        return first[0] != minus && second[0] != minus;
    }

    public string Plus(params string[] additions)
    {
        string currentAddition = additions[0];
        for (int i = 1; i < additions.Length; i++)
        {
            char[] first = currentAddition.ToCharArray();
            Array.Reverse(first);
            char[] second = additions[i].ToCharArray();
            Array.Reverse(second);

            int length = Mathf.Max(first.Length, second.Length);

            int rememberOne = 0;

            List<char> sumList = new List<char>();
            for (int j = 0; j < length; j++)
            {
                int firstNum = GetNumberFromChar(first, j);
                int secondNun = GetNumberFromChar(second, j);
                int intSum = firstNum + secondNun + rememberOne;
                rememberOne = intSum >= 10 ? 1 : 0;
                char num = intSum.ToString().ToCharArray().Last();
                sumList.Add(num);
            }

            if (rememberOne > 0)
                sumList.Add('1');
            sumList.Reverse();
            RemoveAllZerosFromStart(sumList);
            currentAddition = new string(sumList.ToArray());
        }
        return currentAddition;
    }

    private int GetNumberFromChar(char[] charArr, int j)
    {
        int firstNum = 0;
        if (j < charArr.Length)
        {
            firstNum = int.Parse(charArr[j].ToString());
        }
        return firstNum;
    }
}

