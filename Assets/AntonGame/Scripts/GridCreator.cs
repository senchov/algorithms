﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using TMPro;

namespace AntonGame
{
    public class GridCreator : MonoBehaviour
    {
        [SerializeField] private EdgeCollider2D Collider;
        [SerializeField] private Transform[] Points;
        [SerializeField] private Transform Point;
        [SerializeField] private TextMeshPro Label;
        [SerializeField] private int TriangleIndex;

        private Triangle Triangle;
        List<Triangle> Triangles;

        private void Start()
        {
           
            Create();
            GameObject holder = new GameObject("Holder");
            CreatePoint(Triangles[0].A, holder.transform, "A");
            CreatePoint(Triangles[0].B, holder.transform, "B");
            CreatePoint(Triangles[0].C, holder.transform, "C");
        }

        private void Update()
        {
            //Vector2 a = Points[1].position - Points[0].position;
            //Vector2 p = Point.position - Points[0].position;

            //float dotProduct = Vector3.Dot(a, p);

            //Vector3 cross = Vector3.Cross(a, p);
            //float dotCross = Vector3.Dot(a, cross);
           // Debug.Log("cross->" + cross);
            if (IsInsideTriangles(Point.position))
            {
                Debug.LogError("true");
            }
        }


        [ContextMenu("Create")]
        public void Create()
        {
            Triangles = new List<Triangle>();
            List<Vector2> points = Collider.points.ToList();           

            Triangulation triangulation = new Triangulation();
            List<Vector2> triangulatedPoints = triangulation.GetResult(points, false);

            int counter = 0;
            for (int i = 0; i < triangulatedPoints.Count - 3; i += 3)
            {
                Vector3 a = triangulatedPoints[i];
                Vector3 b = triangulatedPoints[i + 1];
                Vector3 c = triangulatedPoints[i + 2];
                Triangle triangle = new Triangle(a, b, c);
                Triangles.Add(triangle);
                counter = InstantiateLabel(counter, a, b, c);
            }
        }

        private int InstantiateLabel(int counter, Vector3 a, Vector3 b, Vector3 c)
        {
            Vector3 center = (a + b + c) / 3;
            TextMeshPro label = Instantiate(Label);
            label.transform.position = center;
            label.gameObject.SetActive(true);
            label.text = counter++.ToString();
            return counter;
        }

        [ContextMenu("FillTriangle")]
        public void FillTriangle() 
        {
            Triangle = Triangles[TriangleIndex];
        }

        private bool IsInsideTriangles(Vector3 point)
        {
            for (int i = 0; i < Triangles.Count; i++)
            {
                if (Triangles[i].IsPointInside(point))
                    return true;
            }

            return false;
        }

        private void CreatePoint(Vector3 pos, Transform holder, string name)
        {
            GameObject point = new GameObject(name);
            point.transform.position = pos;
            point.transform.SetParent(holder);
        }

        private void OnDrawGizmos()
        {
            if (Triangles == null)
                return;

            for (int i = 0; i < Triangles.Count; i++)
            {
                Gizmos.color = new Color(Random.Range(0, 1), Random.Range(0, 1), Random.Range(0, 1), 1);
                Gizmos.DrawLine(Triangles[i].A, Triangles[i].B);
                Gizmos.DrawLine(Triangles[i].B, Triangles[i].C);
                Gizmos.DrawLine(Triangles[i].C, Triangles[i].A);                
            }
        }
    }
}
