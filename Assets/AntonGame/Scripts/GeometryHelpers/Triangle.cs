﻿using UnityEngine;

namespace AntonGame
{
    public class Triangle
    {
        public Vector3 A;
        public Vector3 B;
        public Vector3 C;

        public Triangle(Vector3 a, Vector3 b, Vector3 c)
        {
            Init(a, b, c);
        }

        public void Init(Vector3 a, Vector3 b, Vector3 c)
        {
            A = a;
            B = b;
            C = c;
        }

        public bool IsPointInside(Vector3 point)
        {
            Vector3 firstLine = B - A;
            Vector3 secondLine = C - B;
            Vector3 thirdLine = A - C;

            bool firstCheck = IsSame(firstLine, secondLine, point - A);
            bool secondCheck = IsSame(secondLine, thirdLine, point - B);
            bool thirdCheck = IsSame(thirdLine, firstLine, point - C);

            return firstCheck && secondCheck && thirdCheck;
        }

        private bool IsSame(Vector3 firstTringleLine, Vector3 secondTriangleLine, Vector3 targetLine)
        {
            Vector3 cpFirstTarget = Vector3.Cross(firstTringleLine, targetLine);
            Vector3 cpFirstSecond = Vector3.Cross(firstTringleLine, secondTriangleLine);

            float dot = Vector3.Dot(cpFirstTarget, cpFirstSecond);
            return dot > 0;
        }
    }
}
