﻿using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class NeatViewer : MonoBehaviour
    {
        [SerializeField] private NodeView NodePrefab;
        [SerializeField] private ConnectionView ConnectionPrefab;
        [SerializeField] private Vector2 Size;

        private Dictionary<int, GameObject> NodesByInnovation;
        private List<GameObject> Connections;
        private Genome CurrentGenome;

        public void CreateNetwork()
        {
            NodesByInnovation = new Dictionary<int, GameObject>();
            IRandom random = new UnityRandom();
            Neat neat = new Neat(1, 1, 100, random, new Config());
            //CurrentGenome = neat.EmptyGenome();
            double[] inputs = new double[1];

            for (int i = 0; i < 1; i++)
            {
                inputs[i] = (double)random.NextFloat();
            }

            for (int i = 0; i < 100; i++)
            {
                foreach (Client client in neat.ClientsList)
                {
                    double score = client.Calculate(inputs)[0];
                    client.Score = score;
                }
                neat.Evolve();
                neat.PrintSpecies();
            }
           
           // DisplayNetwork();
        }

        private void DisplayNetwork()
        {
            CreateNodes();
            CreateConnections();
        }

        private void CreateNodes()
        {
            foreach (NodeGene node in CurrentGenome.NodesByInnovation.Values)
            {
                CreateNode(node);
            }
        }

        private void CreateNode(NodeGene node)
        {
            NodeView view = Instantiate(NodePrefab);
            view.Display(node.Innovation);
            Vector3 pos = new Vector3((float)node.x * Size.x, (float)node.y * Size.y, 0);
            view.transform.position = pos;
            NodesByInnovation.Add(node.Innovation, view.gameObject);
        }

        private void CreateConnections()
        {
            Connections = new List<GameObject>();
            foreach (ConnectionGene connection in CurrentGenome.ConnectionsByInnovation.Values)
            {
                Vector3 from = NodesByInnovation[connection.From.Innovation].transform.position;
                Vector3 to = NodesByInnovation[connection.To.Innovation].transform.position;
                ConnectionView view = Instantiate(ConnectionPrefab);
                view.Display(from, to, connection.Weight, connection.IsEnabled);
                Connections.Add(view.gameObject);
            }
        }

        public void AddLinkMutate()
        {
            CurrentGenome.AddLinkMutate();
            CreateConnections();
        }

        public void AddNodeMutate()
        {
            CurrentGenome.AddNodeMutate();
            AddAbsentNodes();
            CrearConnections();
            CreateConnections();
        }

        private void AddAbsentNodes()
        {
            foreach (var kvp in CurrentGenome.NodesByInnovation)
            {
                if (!NodesByInnovation.ContainsKey(kvp.Key))
                    CreateNode(kvp.Value);
            }
        }

        public void WeightShiftMutate()
        {
            CurrentGenome.WeightShiftMutate();
            CrearConnections();
            CreateConnections();
        }

        public void WeightMutate()
        {
            CurrentGenome.WeightMutate();
            CrearConnections();
            CreateConnections();
        }

        public void EnableMutate()
        {
            CurrentGenome.EnableMutate();
            CrearConnections();
            CreateConnections();
        }

        public void Mutate()
        {
            CurrentGenome.Mutate();
            AddAbsentNodes();
            CrearConnections();
            CreateConnections();
        }

        private void ClearNodes()
        {
            foreach (GameObject gameObject in NodesByInnovation.Values)
            {
                Destroy(gameObject);
            }
        }

        private void CrearConnections()
        {
            foreach (GameObject connection in Connections)
            {
                Destroy(connection);
            }
        }

        public void Calculate()
        {
            Calculator calculator = new Calculator(CurrentGenome);
            double[] outputs = calculator.Calculate(1, 1, 1);
            foreach (double output in outputs)
            {
                Debug.Log(output);
            }
        }

       
    }
}
