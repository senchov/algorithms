﻿using UnityEngine;

namespace AI
{
    public class NodeView : MonoBehaviour
    {
        [SerializeField] private TextMesh Label;

        public void Display(int innovation) 
        {
            Label.text = innovation.ToString();
        }
    }
}
