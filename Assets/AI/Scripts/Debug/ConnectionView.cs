﻿using System;
using UnityEngine;

namespace AI
{
    public class ConnectionView : MonoBehaviour
    {
        [SerializeField] private LineRenderer Line;
        [SerializeField] private TextMesh Label;
        [SerializeField] private float LabelOffset = 0.5f;

        public void Display(Vector3 a, Vector3 b, double weight, bool isEnabled)
        {
            Line.SetPosition(0, a);
            Line.SetPosition(1, b);
            Label.transform.position = (a + b) / 2 + new Vector3 (UnityEngine.Random.Range(-LabelOffset, LabelOffset), UnityEngine.Random.Range(-LabelOffset, LabelOffset), 0);
            Label.text = String.Format("{0:0.00}", weight); ;
            Line.material.color = isEnabled ? Color.green : Color.red;            
        }
    }
}
