﻿using UnityEngine;
using UnityEditor;

namespace AI
{
    [CustomEditor(typeof(NeatViewer))]
    public class NeatViewerCustomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Generate")) 
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.CreateNetwork();
            }

            GUILayout.Space(10);

            if (GUILayout.Button("AddLinkMutate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.AddLinkMutate();
            }

            if (GUILayout.Button("AddNodeMutate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.AddNodeMutate();
            }

            if (GUILayout.Button("EnableMutate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.EnableMutate();
            }

            if (GUILayout.Button("WeightMutate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.WeightMutate();
            }

            if (GUILayout.Button("WeightShiftMutate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.WeightShiftMutate();
            }

            if (GUILayout.Button("Mutate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.Mutate();
            }

            GUILayout.Space(10);

            if (GUILayout.Button("Calculate"))
            {
                NeatViewer neatViewer = target as NeatViewer;
                neatViewer.Calculate();
            }

            GUILayout.Space(10);

            if (GUILayout.Button("DebugNetwork"))
            {
                NeatViewer neatViewer = target as NeatViewer;
               // neatViewer.
            }
        }
    } 
}