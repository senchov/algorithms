﻿namespace AI
{
    public class Gene
    {
        public int Innovation;

        public Gene(int innovation)
        {
            Innovation = innovation;
        }
    }
}
