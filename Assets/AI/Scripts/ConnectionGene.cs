﻿namespace AI
{
    public class ConnectionGene : Gene
    {
        public NodeGene From;
        public NodeGene To;
        public double Weight;
        public bool IsEnabled;


        public ConnectionGene(NodeGene from, NodeGene to, int innovation) : base(innovation)
        {
            From = from;
            To = to;
        }

        public override bool Equals(object obj)
        {
            ConnectionGene gene = obj as ConnectionGene;
            if (gene == null)
                return false;
            bool isToEqual = To.Equals(gene.To);
            bool isFromEqual = From.Equals(gene.From);
            return isToEqual && isFromEqual;
        }

        public override string ToString()
        {
            return GetHash(From, To);
        }

        public static string GetHash(NodeGene from, NodeGene to)
        {
            return from.Innovation + "_" + to.Innovation;
        }

        public ConnectionGene Clone()
        {
            ConnectionGene newGene = new ConnectionGene(From, To, Innovation);
            newGene.Weight = Weight;
            newGene.IsEnabled = IsEnabled;
            return newGene;
        }
    }
}
