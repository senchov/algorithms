﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI
{
    public class Genome
    {
        public Dictionary<string, ConnectionGene> ConnectionsByHash;
        public SortedDictionary<int, ConnectionGene> ConnectionsByInnovation;
        public Dictionary<int, NodeGene> NodesByInnovation;
        private Neat Neat;
        public IRandom Random;
        public Config Config;

        public Genome(Neat neat, IRandom random)
        {
            ConnectionsByHash = new Dictionary<string, ConnectionGene>(StringComparer.Ordinal);
            ConnectionsByInnovation = new SortedDictionary<int, ConnectionGene>();
            NodesByInnovation = new Dictionary<int, NodeGene>();
            Neat = neat;
            Random = random;
        }

        // this should have highest innovation number
        public double DistanceTo(Genome b)
        {
            Genome a = this;
            if (a.ConnectionsByInnovation.Count == 0 || b.ConnectionsByInnovation.Count == 0)
                return 0;

            int highInnovation_a = a.ConnectionsByInnovation.Keys.Last();
            int highInnovation_b = b.ConnectionsByInnovation.Keys.Last();
            if (highInnovation_a < highInnovation_b)
            {
                SwapGenomes(ref b, ref a, out highInnovation_a, out highInnovation_b);
            }

            IEnumerator<int> aEnumerator = a.ConnectionsByInnovation.Keys.GetEnumerator();
            IEnumerator<int> bEnumerator = b.ConnectionsByInnovation.Keys.GetEnumerator();

            int matches = 0;
            int disjoint = 0;
            double weight = 0;
            aEnumerator.MoveNext();
            bEnumerator.MoveNext();

            while (aEnumerator.Current < highInnovation_a && bEnumerator.Current < highInnovation_b)
            {
                int index_a = aEnumerator.Current;
                int index_b = bEnumerator.Current;
                ConnectionGene gene_a = a.ConnectionsByInnovation[index_a];
                ConnectionGene gene_b = b.ConnectionsByInnovation[index_b];
                if (index_a == index_b)
                {
                    aEnumerator.MoveNext();
                    bEnumerator.MoveNext();
                    matches++;
                    weight += Math.Abs(gene_a.Weight - gene_b.Weight);
                    continue;
                }

                if (index_a > index_b)
                {
                    bEnumerator.MoveNext();
                    //disjoint gene og b
                    disjoint++;
                }
                else
                {
                    aEnumerator.MoveNext();
                    //disjoint gene of a
                    disjoint++;
                }

            }
            int execess = highInnovation_a - aEnumerator.Current;
            double avarageWeight = matches > 0 ? weight / matches : 0;
            aEnumerator.Dispose();
            bEnumerator.Dispose();

            double N = Mathf.Max(a.ConnectionsByInnovation.Count, b.ConnectionsByInnovation.Count);
            N = N >= 20 ? N : 1;

            return Neat.Config.C1 * disjoint / N + Neat.Config.C2 * execess / N + Neat.Config.C3 * avarageWeight;
        }

        private static void SwapGenomes(ref Genome b, ref Genome a, out int highInnovation_a, out int highInnovation_b)
        {
            Genome temp = a;
            a = b;
            b = temp;
            highInnovation_a = a.ConnectionsByInnovation.Keys.Last();
            highInnovation_b = b.ConnectionsByInnovation.Keys.Last();
        }

        public void Mutate()
        {
            if (Config.ProbAddLink < Random.NextFloat())
            {
                AddLinkMutate();
            }

            if (Config.ProbAddNode < Random.NextFloat())
            {
                AddNodeMutate();
            }

            if (Config.ProbIsEnable < Random.NextFloat())
            {
                EnableMutate();
            }

            if (Config.WeightRandom < Random.NextFloat())
            {
                WeightMutate();
            }

            if (Config.WeightShift < Random.NextFloat())
            {
                WeightShiftMutate();
            }
        }

        public void AddLinkMutate()
        {
            for (int i = 0; i < 100; i++)
            {
                NodeGene a = GetRandomNode();
                NodeGene b = GetRandomNode();
                if (a.x == b.x)
                    continue;

                ConnectionGene connection = a.x < b.x ? new ConnectionGene(a, b, 0) : new ConnectionGene(b, a, 0);
                string hash = connection.ToString();
                if (ConnectionsByHash.ContainsKey(hash))
                    continue;
                connection.Innovation = InnovationCounter.GetInnovation();
                connection.Weight = Random.NextFloat(-1, 1) * Neat.Config.WeightRandom;
                connection.IsEnabled = true;
                AddConnection(connection);
                return;
            }
        }

        public void AddNodeMutate()
        {
            if (ConnectionsByHash.Count == 0)
                return;

            ConnectionGene connection = GetRandomConnection();
            NodeGene from = connection.From;
            NodeGene to = connection.To;

            NodeGene middle = Neat.GetNode();
            middle.x = (from.x + to.x) / 2;
            middle.y = (from.y + to.y) / 2;

            ConnectionGene left = Neat.GetConnection(from, middle);
            ConnectionGene right = Neat.GetConnection(middle, to);
            left.Weight = 1;
            right.Weight = connection.Weight;
            right.IsEnabled = connection.IsEnabled;

            RemoveConnection(connection);
            AddConnection(left);
            AddConnection(right);

            NodesByInnovation.Add(middle.Innovation, middle);
        }

        public void WeightShiftMutate()
        {
            if (ConnectionsByHash.Count == 0)
                return;
            ConnectionGene connection = GetRandomConnection();
            connection.Weight += Random.NextFloat(-1, 1) * Neat.Config.WeightShift;
        }

        public void WeightMutate()
        {
            if (ConnectionsByHash.Count == 0)
                return;
            ConnectionGene connection = GetRandomConnection();
            connection.Weight = Random.NextFloat(-1, 1) * Neat.Config.WeightRandom;
        }

        public void EnableMutate()
        {
            if (ConnectionsByHash.Count == 0)
                return;
            ConnectionGene connection = GetRandomConnection();
            connection.IsEnabled = !connection.IsEnabled;
        }

        private ConnectionGene GetRandomConnection()
        {
            int[] innovations = ConnectionsByInnovation.Keys.ToArray();
            int randomIndex = Random.NextInt(0, innovations.Length);
            int innovationKey = innovations[randomIndex];
            return ConnectionsByInnovation[innovationKey];
        }

        private NodeGene GetRandomNode()
        {
            int[] innovations = NodesByInnovation.Keys.ToArray();
            int randomIndex = Random.NextInt(0, innovations.Length);
            int innovationKey = innovations[randomIndex];
            return NodesByInnovation[innovationKey];
        }

        public void AddConnection(ConnectionGene connection)
        {
            string hash = connection.ToString();
            if (ConnectionsByHash.ContainsKey(hash))
            {
                Debug.LogError("DuplicateConnection -> " + hash);
                return;
            }

            ConnectionsByHash.Add(hash, connection);
            ConnectionsByInnovation.Add(connection.Innovation, connection);
        }

        public void RemoveConnection(ConnectionGene connection)
        {
            string hash = connection.ToString();
            if (!ConnectionsByHash.ContainsKey(hash))
            {
                Debug.LogError("Connection is absent -> " + hash);
                return;
            }
            int innovation = ConnectionsByHash[hash].Innovation;
            ConnectionsByInnovation.Remove(innovation);
            ConnectionsByHash.Remove(hash);
        }

        public Genome CrossOver(Genome b)
        {
            Genome a = this;
            Neat neat = a.Neat;
            Genome genome = neat.EmptyGenome();

            int highInnovation_a = a.ConnectionsByInnovation.Keys.Last();
            int highInnovation_b = b.ConnectionsByInnovation.Keys.Last();

            if (highInnovation_a < highInnovation_b)
            {
                SwapGenomes(ref b, ref a, out highInnovation_a, out highInnovation_b);
            }

            int index_a = a.ConnectionsByInnovation.Keys.First();
            int index_b = b.ConnectionsByInnovation.Keys.First();

            while (index_a <= highInnovation_a && index_b <= highInnovation_b)
            {
                ConnectionGene gene_a = a.ConnectionsByInnovation[index_a];
                ConnectionGene gene_b = b.ConnectionsByInnovation[index_b];

                AddConnectionTo(genome, gene_a, gene_b);

                if (index_a == index_b)
                {
                    index_a++;
                    index_b++;
                    continue;
                }

                if (index_a > index_b)
                {
                    //disjoint gene og b
                    index_b++;
                }
                else
                {
                    //disjoint gene of a
                    index_a++;
                }
            }

            while (index_a <= highInnovation_a)
            {
                if (a.ConnectionsByInnovation.ContainsKey(index_a))
                {
                    ConnectionGene connectionGene = a.ConnectionsByInnovation[index_a].Clone();
                    genome.AddConnection(connectionGene);
                }
                index_a++;
            }

            foreach (ConnectionGene connection in genome.ConnectionsByInnovation.Values)
            {
                AddNodeTo(genome, connection);
            }

            return genome;
        }

        private void AddConnectionTo(Genome genome, ConnectionGene gene_a, ConnectionGene gene_b)
        {
            if (Random.NextFloat() > 0.5f)
            {
                ConnectionGene connection = gene_a.Clone();
                genome.AddConnection(connection);
            }
            else
            {
                ConnectionGene connection = gene_b.Clone();
                genome.AddConnection(connection);
            }
        }

        private void AddNodeTo(Genome genome, ConnectionGene connection)
        {
            genome.NodesByInnovation.Add(connection.From.Innovation, connection.From);
            genome.NodesByInnovation.Add(connection.To.Innovation, connection.To);
        }
    }
}
