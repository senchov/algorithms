﻿namespace AI
{
    public class NodeGene : Gene
    {
        public double x, y;

        public NodeGene(int innovation) : base(innovation)
        {           
        }

        public override bool Equals(object obj)
        {
            NodeGene gene = obj as NodeGene;
            if (gene == null)
                return false;
            return Innovation == gene.Innovation;
        }
    }
}
