﻿namespace AI
{
    public static class InnovationCounter
    {
        private static int CurrentInnovation;

        public static int GetInnovation()
        {
            return CurrentInnovation++;
        }
    }
}
