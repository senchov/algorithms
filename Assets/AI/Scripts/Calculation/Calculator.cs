﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI
{
    public class Calculator
    {
        private Genome Genome;

        private List<Node> Inputs;
        private List<Node> Hidden;
        private List<Node> Outputs;

        public Calculator(Genome genome)
        {
            Genome = genome;
            Inputs = new List<Node>();
            Hidden = new List<Node>();
            Outputs = new List<Node>();

            Dictionary<int, Node> nodesByInnovation = new Dictionary<int, Node>();
            FillData(genome, nodesByInnovation);
            Hidden = Hidden.OrderBy(n => n.X).ToList();
            FillConnections(genome, nodesByInnovation);
        }      

        private void FillData(Genome genome, Dictionary<int, Node> nodesByInnovation)
        {
            foreach (NodeGene nodeGene in genome.NodesByInnovation.Values)
            {
                Node node = new Node(nodeGene.x);
                nodesByInnovation.Add(nodeGene.Innovation, node);
                if (nodeGene.x <= 0)
                {
                    Inputs.Add(node);
                    continue;
                }

                if (1 <= nodeGene.x)
                {
                    Outputs.Add(node);
                    continue;
                }
                Hidden.Add(node);
            }
        }

        private void FillConnections(Genome genome, Dictionary<int, Node> nodesByInnovation)
        {
            foreach (ConnectionGene connection in genome.ConnectionsByInnovation.Values)
            {
                Node from = nodesByInnovation[connection.From.Innovation];
                Node to = nodesByInnovation[connection.To.Innovation];
                Connection newConnection = new Connection(from, to);
                newConnection.Weight = connection.Weight;
                newConnection.IsEnabled = connection.IsEnabled;
                to.Connections.Add(newConnection);
            }
        }

        public double[] Calculate(params double[] inputs)
        {
            if (inputs.Length != Inputs.Count)
            {
                Debug.LogError("Smt goes wrong");
                return null;
            }
            SetInputsOutput(inputs);
            CalculateHiddenNodes();

            double[] outputs = new double[Outputs.Count];
            for (int i = 0; i < Outputs.Count; i++)
            {
                Outputs[i].Calculate();
                outputs[i] = Outputs[i].Output;
            }
            return outputs;
        }
        private void SetInputsOutput(double[] inputs)
        {
            for (int i = 0; i < Inputs.Count; i++)
            {
                Inputs[i].Output = inputs[i];
            }
        }

        private void CalculateHiddenNodes()
        {
            foreach (Node hidden in Hidden)
            {
                hidden.Calculate();
            }
        }
      
    }
}
