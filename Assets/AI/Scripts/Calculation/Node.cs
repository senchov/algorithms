﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class Node
    {
        public double X;
        public double Output;
        public List<Connection> Connections;

        public Node(double x)
        {
            X = x;
            Connections = new List<Connection>();
        }

        public void Calculate()
        {
            double sum = 0;
            foreach (Connection connection in Connections)
            {
                if (connection.IsEnabled)
                {
                    sum += connection.Weight * connection.From.Output;
                }
            }
            Output = ActivationFunction(sum);
        }

        private double ActivationFunction(double value)
        {
            return 1d / (1 + Math.Exp(-value));
        }
    }
}
