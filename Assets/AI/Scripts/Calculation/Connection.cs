﻿namespace AI
{
    public class Connection
    {
        public Node From;
        public Node To;
        public double Weight;
        public bool IsEnabled;


        public Connection(Node from, Node to) //: base(innovation)
        {
            From = from;
            To = to;
        }

        public override bool Equals(object obj)
        {
            ConnectionGene gene = obj as ConnectionGene;
            if (gene == null)
                return false;
            bool isToEqual = To.Equals(gene.To);
            bool isFromEqual = From.Equals(gene.From);
            return isToEqual && isFromEqual;
        }
    }
}
