﻿namespace AI
{
    public class Client
    {
        private Calculator Calculator;

        public Genome Genome;
        public double Score;
        public Species Spicies;

        public Client(Genome genome)
        {
            Genome = genome;
            Calculator = new Calculator(Genome);
        }

        public double[] Calculate(params double[] inputs)
        {
            return Calculator.Calculate(inputs);
        }

        public double Distance(Client client)
        {
            return Genome.DistanceTo(client.Genome);
        }

        public void Mutate()
        {
            Genome.Mutate();
        }

        public Config Config { get { return Genome.Config; } }
        public IRandom Random { get { return Genome.Random; } }
    }
}
