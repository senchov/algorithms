﻿namespace AI
{
    public class Config
    {
        public int MaxNodes = 1000000;
        public double C1 = 1;
        public double C2 = 1;
        public double C3 = 1;
        public double WeightShift = 0.3d;
        public double WeightRandom = 1;

        public double ProbAddLink = 0.1f;
        public double ProbAddNode = 0.05f;
        public double ProbWeightShift = 0.9f;
        public double ProbWeight = 0.45f;
        public double ProbIsEnable = 0.15f;

        public double CompabilityTreshold = 4;

        public float KillPercantage = 0.5f;
    }
}
