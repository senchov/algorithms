﻿using System.Collections.Generic;


namespace AI
{
    public class RandomSelector<T> where T : class
    {
        private List<T> Nodes;
        private List<double> Weights;

        private double TotalWeight;
        private IRandom Random;

        public RandomSelector(IRandom random)
        {
            Nodes = new List<T>();
            Weights = new List<double>();
            Random = random;
        }

        public void Add(T node, double score)
        {
            Nodes.Add(node);
            Weights.Add(score);
            TotalWeight += score;
        }

        public T GetNode()
        {
            double randomValue = (double)(Random.NextFloat(0, 1.0f) * TotalWeight);

            double sum = 0;
            for (int i = 0; i < Nodes.Count; i++)
            {
                sum += Weights[i];
                if (sum >= randomValue)
                    return Nodes[i];
            }
            return null;
        }

        public void Reset() 
        {
            Nodes = new List<T>();
            Weights = new List<double>();
            TotalWeight = 0;
        }
    }
}
