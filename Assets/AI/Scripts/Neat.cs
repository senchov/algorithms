﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class Neat
    {
        private Dictionary<string, ConnectionGene> AllConnections;
        private Dictionary<int, NodeGene> AllNodesByInnovation;

        public List<Client> ClientsList;
        public List<Species> SpeciesList;

        private int Inputs;
        private int Outputs;
        private int Clients;
        private IRandom Random;
        public Config Config;

        public Neat(int inputs, int outputs, int clients, IRandom random, Config config)
        {
            AllConnections = new Dictionary<string, ConnectionGene>();
            AllNodesByInnovation = new Dictionary<int, NodeGene>();

            Inputs = inputs;
            Outputs = outputs;
            Clients = clients;

            //FillWithInputsAndOutputsNodes(inputs, outputs);
            Random = random;
            Config = config;

            ClientsList = new List<Client>();
            for (int i = 0; i < clients; i++)
            {
                Genome genome = EmptyGenome();
                Client client = new Client(genome);
                ClientsList.Add(client);
            }
            SpeciesList = new List<Species>();
        }

        public Client GetClient(int index)
        {
            if (index < 0 && ClientsList.Count <= index)
            {
                Debug.LogError("Wrong index->" + index);
                return null;
            }
            return ClientsList[index];
        }

        //private void FillWithInputsAndOutputsNodes(int inputs, int outputs)
        //{
        //    for (int i = 0; i < inputs; i++)
        //    {
        //        NodeGene node = GetNode();
        //        node.x =0;
        //        node.y = (i + 1) / (inputs + 1);
        //    }

        //    for (int i = 0; i < outputs; i++)
        //    {
        //        NodeGene node = GetNode();
        //        node.x = 1;
        //        node.y = (i + 1) / (outputs + 1);
        //    }
        //}

        public NodeGene GetNode()
        {
            int innovationNumber = InnovationCounter.GetInnovation();
            NodeGene node = new NodeGene(innovationNumber);
            AllNodesByInnovation.Add(innovationNumber, node);
            return node;
        }

        public NodeGene GetNode(int innovation)
        {
            if (AllNodesByInnovation.ContainsKey(innovation))
                return AllNodesByInnovation[innovation];
            return new NodeGene(innovation);
        }

        public Genome EmptyGenome()
        {
            Genome genome = new Genome(this, Random);
            genome.Config = new Config();
            FillWithNodes(genome, Inputs, 0);
            FillWithNodes(genome, Outputs, 1);
            return genome;
        }

        private void FillWithNodes(Genome genome, int amount, double x)
        {
            for (int i = 0; i < amount; i++)
            {
                int innovation = InnovationCounter.GetInnovation();
                NodeGene node = GetNode(innovation);
                node.x = x;
                node.y = (double)(i + 1) / (double)(amount + 1);
                genome.NodesByInnovation.Add(innovation, node);
            }
        }

        public ConnectionGene GetConnection(NodeGene from, NodeGene to)
        {
            string hash = ConnectionGene.GetHash(from, to);
            ConnectionGene connectionGene = new ConnectionGene(from, to, 0);

            if (AllConnections.ContainsKey(hash))
            {
                int innovation = AllConnections[hash].Innovation;
                connectionGene.Innovation = innovation;
            }
            else
            {
                int innovation = InnovationCounter.GetInnovation();
                connectionGene.Innovation = innovation;
                AllConnections.Add(hash, connectionGene);
            }
            return connectionGene;
        }

        public int GetConnectionInnovation(NodeGene from, NodeGene to)
        {
            string hash = ConnectionGene.GetHash(from, to);
            if (AllConnections.ContainsKey(hash))
            {
                return AllConnections[hash].Innovation;
            }

            return -1;
        }

        public void Evolve()
        {
            GenerateSpecies();
            Kill();
            RemoveExtinctSpicies();
            Reproduce();
            Mutate();
        }

        private void GenerateSpecies()
        {
            foreach (Species species in SpeciesList)
            {
                species.Reset();
            }

            foreach (Client client in ClientsList)
            {
                if (client.Spicies != null)
                    continue;
                bool isFound = false;
                foreach (Species species in SpeciesList)
                {
                    if (species.AddIfCould(client))
                    {
                        isFound = true;
                        break;
                    }
                }

                if (!isFound)
                {
                    Species newSpecies = new Species(client);
                    SpeciesList.Add(newSpecies);
                }
            }

            foreach (Species species in SpeciesList)
            {
                species.EvaluateScore();
            }
        }

        private void Kill()
        {
            foreach (Species species in SpeciesList)
            {
                species.Kill(Config.KillPercantage);
            }
        }

        private void RemoveExtinctSpicies()
        {
            for (int i = SpeciesList.Count - 1; i >= 0; i--)
            {
                if (SpeciesList[i].Size <= 1)
                {
                    SpeciesList[i].Extinct();
                    SpeciesList.RemoveAt(i);
                }
            }
        }

        private void Reproduce()
        {
            RandomSelector<Species> selector = new RandomSelector<Species>(Random);
            foreach (Species species in SpeciesList)
            {
                selector.Add(species, species.Score);
            }

            foreach (Client client in ClientsList)
            {
                if (client.Spicies == null)
                {
                    Species spicies = selector.GetNode();
                    client.Genome = spicies.Breed();
                    spicies.ForceAdd(client);
                }
            }
        }

        private void Mutate()
        {
            foreach (Client client in ClientsList)
            {
                client.Mutate();
            }
        }

        public void PrintSpecies()
        {
            Debug.Log("-------------------------");
            foreach (Species species in SpeciesList)
            {
                Debug.Log(species.GetHashCode() + "  " + species.Score + " " + species.Size);
            }
        }
    }
}
