﻿using NUnit.Framework;
using UnityEngine;

namespace AI
{
    public class AITests
    {
        [Test]
        public void First()
        {
            Neat neat = new Neat(3, 3, 100, new UnityRandom(), new Config());
            Genome genome = neat.EmptyGenome();
            Debug.Log(genome.NodesByInnovation.Count);
            Assert.AreEqual(6, genome.NodesByInnovation.Count);
        }
    }
}
