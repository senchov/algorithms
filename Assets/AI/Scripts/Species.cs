﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI
{
    public class Species
    {
        private List<Client> Clients;
        private Client Representative;
        public double Score;
        private IRandom Random;

        public Species(Client representative)
        {
            Representative = representative;
            Representative.Spicies = this;
            Clients = new List<Client>();
            Clients.Add(representative);
            Random = representative.Random;
        }

        public bool AddIfCould(Client client)
        {
            double cp = Representative.Config.CompabilityTreshold;
            if (client.Distance(Representative) < cp)
            {
                Clients.Add(client);
                client.Spicies = this;
                return true;
            }
            return false;
        }

        public void ForceAdd(Client client)
        {
            Clients.Add(client);
            client.Spicies = this;
        }

        public void Extinct()
        {
            foreach (Client client in Clients)
            {
                client.Spicies = null;
            }
        }

        public void EvaluateScore()
        {
            if (Clients.Count == 0)
                return;
            double scoreSum = Clients.Sum(c => c.Score);
            Score = scoreSum / Clients.Count;
        }

        public void Reset()
        {
            Representative = Clients[Random.NextInt(0, Clients.Count)];
            Extinct();
            Clients = new List<Client>();
            Clients.Add(Representative);
            Representative.Spicies = this;
            Score = 0;
        }

        public void Kill(float percentage)
        {
            int startIndex = (int)(Clients.Count * percentage);
            Clients = Clients.OrderBy(c => c.Score).Skip(startIndex).ToList();
        }

        public Genome Breed()
        {
            Client first = Clients[Random.NextInt(0, Clients.Count)];
            Client second = Clients[Random.NextInt(0, Clients.Count)];
            if (first.Score > second.Score)
            {
                return first.Genome.CrossOver(second.Genome);
            }
            else
            {
                return second.Genome.CrossOver(first.Genome);
            }
        }

        public int Size { get { return Clients.Count; } }
    }
}
