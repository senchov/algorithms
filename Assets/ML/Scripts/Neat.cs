﻿using System.Collections.Generic;
using UnityEngine;

namespace ML
{
    public class Neat
    {
        private List<Species> SpeciesList;
        private List<Organism> Organisms;
        private IRandom Random;
        private Config Config;
        private GenesStorage Storage;
        private GenomeComparator GenomeComparator;
        private CrossOver CrossOver;

        public Neat(IRandom random, Config config)
        {
            SpeciesList = new List<Species>();
            Random = random;
            Config = config;
            Storage = new GenesStorage();
            Organisms = new List<Organism>();
            GenomeComparator = new GenomeComparator(config);
            CrossOver = new CrossOver(Storage, config, random);
            CreatePopulation();
        }

        private void CreatePopulation()
        {
            for (int i = 0; i < Config.Population; i++)
            {
                Genome genome = new Genome(Storage, Config, Random);
                Organism organism = new Organism(genome);
                Organisms.Add(organism);
            }
        }

        public List<float[]> CalculateOutputs(List<float[]> inputs)
        {
            if (inputs.Count != Organisms.Count)
            {
                Debug.LogError("Smt goes wrong!!!");
                return null;
            }

            List<float[]> outputs = new List<float[]>();
            for (int i = 0; i < inputs.Count; i++)
            {
                float[] result = Organisms[i].Calculate(inputs[i]);
                outputs.Add(result);
            }
            return outputs;
        }

        public void SetupScores(List<int> scores)
        {
            for (int i = 0; i < scores.Count; i++)
            {
                Organisms[i].Score += scores[i];
            }
        }

        public void Evolve() 
        {
            AssignToSpecies();
            Kill(Config.KillPercantage);
            Reproduce();
            Mutate();
        }

        private void AssignToSpecies()
        {
            foreach (Organism organism in Organisms)
            {
                bool isFoundSpecies = false;
                foreach (Species species in SpeciesList)
                {
                    Genome fittest = species.Fittest.Genome;
                    if (GenomeComparator.IsFromOneSpicies(organism.Genome, fittest))
                    {
                        species.Add(organism);
                    }
                }

                if (!isFoundSpecies)
                {
                    Species newSpecies = new Species(organism);
                    SpeciesList.Add(newSpecies);
                }
            }
        }

        private void Kill(float percantage)
        {
            for (int i = SpeciesList.Count - 1; i >= 0; i--)
            {
                Species species = SpeciesList[i];
                if (species.Size == 0)
                    SpeciesList.RemoveAt(i);
                species.Kill(percantage);
            }
        }

        private void Reproduce()
        {
            while (Organisms.Count < Config.Population)
            {
                int randomIndex = Random.NextInt(0, SpeciesList.Count);
                Species randomSpicies = SpeciesList[randomIndex];
                RandomSelector<int> randomSelector = new RandomSelector<int>(Random);
                for (int i = 0; i < randomSpicies.Organisms.Count; i++)
                {
                    int score = randomSpicies.Organisms[i].Score;
                    randomSelector.Add(i, score);
                }
                int firstParent = randomSelector.GetNode();

                randomSelector.Reset();
                for (int i = 0; i < randomSpicies.Organisms.Count; i++)
                {
                    if (i == firstParent)
                        continue;
                    int score = randomSpicies.Organisms[i].Score;
                    randomSelector.Add(i, score);
                }
                int secondParent = randomSelector.GetNode();

                Organism first = randomSpicies.Organisms[firstParent];
                Organism second = randomSpicies.Organisms[secondParent];
                Genome childGenome = GetChildGenome(first, second);
                Organism child = new Organism(childGenome);
                child.Score = (first.Score + second.Score) / 2;
                Organisms.Add(child);
            }
        }

        private Genome GetChildGenome(Organism first, Organism second)
        {
            if (first.Score == second.Score)
                return CrossOver.DoWithEqualScores(first.Genome, second.Genome);

            return second.Score < first.Score ? CrossOver.Do(first.Genome, second.Genome) : CrossOver.Do(second.Genome, first.Genome);
        }

        public void Mutate()
        {
            foreach (Organism organism in Organisms)
            {
                Genome genome = organism.Genome;

                if (Random.NextFloat() < Config.ProbWeight)
                    genome.MutateWeight();

                if (Random.NextFloat() < Config.ProbWeightShift)
                    genome.MutateWeightShift();

                if (Random.NextFloat() < Config.ProbIsEnable)
                    genome.MutateIsEnable();

                if (Random.NextFloat() < Config.ProbAddNode)
                {
                    MutateAddNode(genome);
                }

                if (Random.NextFloat() < Config.ProbAddLink)
                {
                    MutateAddConnection(genome);
                }
            }
        }

        private void MutateAddNode(Genome genome)
        {
            ConnectionGene connection = genome.GetRandomConnection();
            if (connection != null)
            {
                NodeGene middle = Storage.GetNode(connection.From, connection.To);
                if (middle == null)
                {
                    middle = Storage.CreateNode(connection.From, connection.To);
                }
                genome.AddHiddenNode(middle);

                ConnectionGene fromToMiddle = Storage.GetConnection(connection.From, middle.Id);
                if (fromToMiddle == null)
                    fromToMiddle = Storage.CreateConnection(connection.From, middle.Id);
                fromToMiddle.Weight = 1;
                fromToMiddle.IsEnabled = true;

                ConnectionGene middleToTo = Storage.GetConnection(middle.Id, connection.To);
                if (middleToTo == null)
                    middleToTo = Storage.CreateConnection(middle.Id, connection.To);
                middleToTo.Weight = connection.Weight;
                middleToTo.IsEnabled = connection.IsEnabled;

                genome.AddConnection(fromToMiddle);
                genome.AddConnection(middleToTo);

                connection.IsEnabled = false;
                genome.RemoveConnection(connection);
            }
        }

        private void MutateAddConnection(Genome genome)
        {
            NodeGene a = genome.GetRandomNode();
            NodeGene b = genome.GetRandomNode(a.Id);

            ConnectionGene ab = Storage.GetConnection(a.Id, b.Id);
            if (ab != null)
            {
                ab.IsEnabled = true;
                ab.Weight = Random.NextFloat(-1, 1) * Config.WeightRandom;
                genome.AddConnection(ab);
                return;
            }
            ConnectionGene ba = Storage.GetConnection(b.Id, a.Id);
            if (ba != null)
            {
                ba.IsEnabled = true;
                ba.Weight = Random.NextFloat(-1, 1) * Config.WeightRandom;
                genome.AddConnection(ba);
                return;
            }

            ConnectionGene newConnection = null;
            if (a.Pos.x < b.Pos.x)
                newConnection = Storage.CreateConnection(a.Id, b.Id);
            else
                newConnection = Storage.CreateConnection(b.Id, a.Id);
            genome.AddConnection(newConnection);
        }
    }
}
