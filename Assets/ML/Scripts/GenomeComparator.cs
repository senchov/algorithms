﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ML
{
    public class GenomeComparator
    {
        private Config Config;

        public GenomeComparator(Config config)
        {
            Config = config;
        }

        public bool IsFromOneSpicies(Genome first, Genome second)
        {
            int lastSecondInnovation = second.GenesInnovations.Last();

            float disjoint = 0;
            float execess = 0;

            IEnumerable firstUniqueGenes = first.GenesInnovations.Except(second.GenesInnovations);
            foreach (int innovation in firstUniqueGenes)
            {
                if (innovation < lastSecondInnovation)
                    disjoint += 1;
                else
                    execess += 1;
            }

            float avarageWeight = GetAverageWeight(first, second);
            float n = Math.Max(first.GenesInnovations.Count, second.GenesInnovations.Count);
            n = n < 20 ? 1 : n;

            float distance = CalulateDistance(disjoint, execess, avarageWeight, n);

            return distance < Config.CompabilityTreshold;
        }

        private float GetAverageWeight(Genome first, Genome second)
        {
            List<int> matches = first.GenesInnovations.Intersect(second.GenesInnovations).ToList();
            if (matches.Count == 0)
                return 0;

            float weight = 0;
            foreach (int innovation in matches)
            {
                Gene gene = first.GetGene(innovation);
                if (gene is NodeGene)
                    continue;
                ConnectionGene firstConnection = first.GetGene(innovation) as ConnectionGene;
                ConnectionGene secondConnection = second.GetGene(innovation) as ConnectionGene;
                weight += Math.Abs(firstConnection.Weight - secondConnection.Weight);
            }
            return weight / matches.Count;
        }

        private float CalulateDistance(float disjoint, float execess, float avarageWeight, float n)
        {
            return Config.C1 * disjoint / n + Config.C2 * execess / n + Config.C3 * avarageWeight;
        }
    }
}
