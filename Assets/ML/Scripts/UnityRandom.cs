﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ML
{
    public class UnityRandom : IRandom
    {
        public bool NextBool()
        {
            int random = Random.Range(0, 2);
            return Convert.ToBoolean(random);
        }

        public float NextFloat()
        {
            return NextFloat(0, 1.0f);
        }

        public float NextFloat(float min, float max)
        {
            return Random.Range(min, max);
        }

        public int NextInt()
        {
            return Random.Range(int.MinValue, int.MaxValue);
        }

        public int NextInt(int min, int max)
        {
            return Random.Range(min, max);
        }
    }
}
