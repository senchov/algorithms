﻿using System.Collections.Generic;
using System.Linq;

namespace ML
{
    public class Species
    {
        public List<Organism> Organisms;
        public Organism Fittest;

        public Species(Organism organism)
        {
            Fittest = organism;
            Organisms = new List<Organism>();
        }

        public void Add(Organism organism)
        {
            Organisms.Add(organism);
            Organisms = Organisms.OrderBy(x => x.Score).ToList();
            Fittest = Organisms.Last();
        }

        public void Kill(float percentage)
        {
            int skipIndex = (int)(Organisms.Count * percentage);
            Organisms = Organisms.Skip(skipIndex).ToList();
        }

        public int Size { get { return Organisms.Count; } }
    }
}
