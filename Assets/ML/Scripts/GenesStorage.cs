﻿using System.Collections.Generic;
using UnityEngine;

namespace ML
{
    public class GenesStorage
    {
        private Dictionary<string, ConnectionGene> ConnectionByHash;
        private Dictionary<string, NodeGene> NodeByHash;
        private Dictionary<int, NodeGene> NodeById;

        public GenesStorage()
        {
            ConnectionByHash = new Dictionary<string, ConnectionGene>();
            NodeByHash = new Dictionary<string, NodeGene>();
            NodeById = new Dictionary<int, NodeGene>();
        }

        public NodeGene CreateNode() 
        {
            NodeGene node = new NodeGene(Counter.GetNodeId(), Counter.GetInnovation());
            return node;
        }

        public NodeGene CreateNode(int from, int to)
        {
            NodeGene node = CreateNode();
            
            NodeByHash.Add(Helper.GetHash(from, to), node);
            NodeById.Add(node.Id, node);
            Vector2 fromPos = NodeById[from].Pos;
            Vector2 toPos = NodeById[to].Pos;
            node.Pos = (fromPos + toPos) / 2;
            return node;
        }

        public NodeGene GetNode(int from, int to) 
        {
            string hash = Helper.GetHash(from, to);
            if (NodeByHash.ContainsKey(hash))
                return NodeByHash[hash];
            return null;
        }

        public ConnectionGene CreateConnection(int from, int to) 
        {
            ConnectionGene connection = new ConnectionGene();
            connection.Id = Counter.GetConnectionId();
            connection.From = from;
            connection.To = to;
            connection.Weight = 1;
            connection.IsEnabled = true;
            ConnectionByHash.Add(Helper.GetHash(from, to), connection);
            return connection;
        }

        public ConnectionGene GetConnection(int from, int to)
        {
            string hash = Helper.GetHash(from, to);
            if (ConnectionByHash.ContainsKey(hash))
                return ConnectionByHash[hash];
            return null;
        }
    }
}
