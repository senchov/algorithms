﻿using UnityEngine;
using System.Collections.Generic;

namespace ML
{
    public class NeatController : MonoBehaviour
    {
        [SerializeField] private Config Config;
        [SerializeField] private float[] Inputs;
        [SerializeField] List<int> Scores;

        private Neat Neat;

        public void CreateNeat()
        {
            IRandom random = new UnityRandom();
            Neat = new Neat(random, Config);
        }

        public void CalculateInputs() 
        {
            List<float[]> inputs = new List<float[]>();
            inputs.Add(Inputs);
            List<float[]> outputs = Neat.CalculateOutputs(inputs);
            foreach (float output in outputs[0])
            {
                Debug.Log(output);
            }
        }

        public void AssignScores()
        {
            Neat.SetupScores(Scores);
        }

        public void Evolve()
        {
            Neat.Evolve();
        }

    } 
}
