﻿using UnityEngine;
using UnityEditor;

namespace ML
{
    [CustomEditor(typeof(NeatController))]
    public class NeatControllerCustomInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("CreateNeat"))
            {
                NeatController cntrl = target as NeatController;
                cntrl.CreateNeat();
            }

            if (GUILayout.Button("Calculate"))
            {
                NeatController cntrl = target as NeatController;
                cntrl.CalculateInputs();
            }

            if (GUILayout.Button("AssignScores"))
            {
                NeatController cntrl = target as NeatController;
                cntrl.AssignScores();
            }

            if (GUILayout.Button("Evolve"))
            {
                NeatController cntrl = target as NeatController;
                cntrl.Evolve();
            }

        }
    }
}