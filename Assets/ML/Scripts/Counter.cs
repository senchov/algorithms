﻿namespace ML
{
    public class Counter
    {
        private static int CurrentInnovation;
        private static int CurrentNodeId;
        private static int CurrentConnectionId;

        public static int GetInnovation()
        {
            return CurrentInnovation++;
        }

        public static int GetNodeId()
        {
            return CurrentNodeId++;
        }

        public static int GetConnectionId()
        {
            return CurrentConnectionId++;
        }
    }
}
