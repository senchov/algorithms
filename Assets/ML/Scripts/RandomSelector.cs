﻿using System.Collections.Generic;

namespace ML
{
    public class RandomSelector<T>
    {
        private List<T> Nodes;
        private List<float> Weights;

        private float TotalWeight;
        private IRandom Random;

        public RandomSelector(IRandom random)
        {
            Nodes = new List<T>();
            Weights = new List<float>();
            Random = random;
        }

        public void Add(T node, float score)
        {
            Nodes.Add(node);
            Weights.Add(score);
            TotalWeight += score;
        }

        public T GetNode()
        {
            float randomValue = Random.NextFloat(0, 1.0f) * TotalWeight;

            float sum = 0;
            for (int i = 0; i < Nodes.Count; i++)
            {
                sum += Weights[i];
                if (sum >= randomValue)
                    return Nodes[i];
            }
            return default(T);
        }

        public void Reset()
        {
            Nodes = new List<T>();
            Weights = new List<float>();
            TotalWeight = 0;
        }
    }
}
