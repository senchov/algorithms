﻿namespace ML
{
    public class ConnectionGene : Gene
    {       
        public int From;
        public int To;
        public float Weight;
        public bool IsEnabled;
    }
}
