﻿using System.Collections.Generic;
using UnityEngine;

namespace ML
{
    public class NodeGene : Gene
    {      
        public List<ConnectionGene> Connections;
        public Vector2 Pos;
        public float Output;

        public NodeGene(int id, int innovation)
        {
            Id = id;
            Innovation = innovation;
            Connections = new List<ConnectionGene>();
        }

        public void AddConnection(ConnectionGene connection) 
        {
            Connections.Add(connection);
        }

        public void RemoveConnection(ConnectionGene connection)
        {
            Connections.Remove(connection);
        }
    }
}
