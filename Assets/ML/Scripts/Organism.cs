﻿namespace ML
{
    public class Organism
    {
        public Genome Genome;
        private Calculator Calculator;

        public int Score;

        public Organism(Genome genome)
        {
            Genome = genome;
            Calculator = new Calculator(genome);
        }

        public float[] Calculate(params float[] inputs)
        {
            return Calculator.Calculate(inputs);
        }
    }
}
