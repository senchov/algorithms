﻿using System.Collections;
using System.Linq;

namespace ML
{
    public class CrossOver
    {
        private Config Config;
        private IRandom Random;
        private GenesStorage Storage;

        public CrossOver(GenesStorage storage, Config config, IRandom random)
        {
            Config = config;
            Random = random;
            Storage = storage;
        }

        public Genome Do(Genome fittest, Genome weaker) 
        {
            Genome child = new Genome(Storage, Config, Random);

            IEnumerable matches = fittest.GenesInnovations.Intersect(weaker.GenesInnovations);
            foreach (int innovation in matches)
            {
                Gene gene = Random.NextBool() ? fittest.GetGene(innovation) : weaker.GetGene(innovation);
                child.AddGene(gene);
            }

            IEnumerable disjointAndExecess = fittest.GenesInnovations.Except(weaker.GenesInnovations);
            foreach (int innovation in disjointAndExecess)
            {
                Gene gene = fittest.GetGene(innovation);
                child.AddGene(gene);
            }

            return child;
        }

        public Genome DoWithEqualScores(Genome first, Genome second) 
        {
            Genome child = new Genome(Storage, Config, Random);

            IEnumerable matches = first.GenesInnovations.Intersect(second.GenesInnovations);
            foreach (int innovation in matches)
            {
                Gene gene = Random.NextBool() ? first.GetGene(innovation) : second.GetGene(innovation);
                child.AddGene(gene);
            }

            IEnumerable firstUniqueGenes = first.GenesInnovations.Except(second.GenesInnovations);
            foreach (int innovation in firstUniqueGenes)
            {
                Gene gene = first.GetGene(innovation);
                child.AddGene(gene);
            }

            IEnumerable secondUniqueGenes = second.GenesInnovations.Except(first.GenesInnovations);
            foreach (int innovation in firstUniqueGenes)
            {
                Gene gene = second.GetGene(innovation);
                child.AddGene(gene);
            }

            return child;
        }
    }
}
