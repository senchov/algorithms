﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ML
{
    public class Genome
    {
        private GenesStorage Storage;

        private Dictionary<int, NodeGene> NodesByInnovation;
        private Dictionary<int, NodeGene> NodesByIds;
        private Dictionary<int, ConnectionGene> ConnectionsByInnovation;
        public List<NodeGene> Inputs;
        public List<NodeGene> Hidden;
        public List<NodeGene> Outputs;
        private Config Config;
        private IRandom Random;
        public List<int> GenesInnovations;

        public Genome(GenesStorage storage, Config config, IRandom random)
        {
            Storage = storage;
            NodesByInnovation = new Dictionary<int, NodeGene>();
            NodesByIds = new Dictionary<int, NodeGene>();
            ConnectionsByInnovation = new Dictionary<int, ConnectionGene>();
            Config = config;      
            Hidden = new List<NodeGene>();
            GenesInnovations = new List<int>();
            Random = random;

            Inputs = new List<NodeGene>();
            FillWithNodes(Inputs, Config.Inputs, 0);
            Outputs = new List<NodeGene>();
            FillWithNodes(Outputs, Config.Outputs, 1);
        }

        private void FillWithNodes(List<NodeGene> nodeList, int amount, float x)
        {
            for (int i = 0; i < amount; i++)
            {
                NodeGene node = Storage.CreateNode();
                Vector2 pos = new Vector2(x, (i + 1) / (amount + 1));
                node.Pos = pos;
                nodeList.Add(node);
                AddNode(node);
            }
        }

        public void AddNode(NodeGene node)
        {
            NodesByIds.Add(node.Id, node);
            NodesByInnovation.Add(node.Innovation, node);
            GenesInnovations.Add(node.Innovation);
            GenesInnovations = GenesInnovations.OrderBy(x => x).ToList();
        }

        public void AddHiddenNode(NodeGene node)
        {
            Hidden.Add(node);
            AddNode(node);
        }

        public void AddConnection(ConnectionGene connectionGene)
        {
            ConnectionsByInnovation.Add(connectionGene.Innovation, connectionGene);
            GenesInnovations.Add(connectionGene.Innovation);
            GenesInnovations = GenesInnovations.OrderBy(x => x).ToList();
            NodeGene from = NodesByIds[connectionGene.Id];
            from.AddConnection(connectionGene);
        }

        public void RemoveConnection(ConnectionGene connectionGene) 
        {
            ConnectionsByInnovation.Remove(connectionGene.Innovation);
            GenesInnovations.Remove(connectionGene.Innovation);
            NodeGene from = NodesByIds[connectionGene.Id];
            from.RemoveConnection(connectionGene);
        }

        public NodeGene GetRandomNode(int exept = -1)
        {
            List<int> ids = NodesByIds.Keys.ToList();
            if (exept != -1)
                ids.Remove(exept);
            int randomId = Random.NextInt(0, ids.Count);
            return NodesByIds[randomId];
        }

        public ConnectionGene GetRandomConnection()
        {
            if (ConnectionsByInnovation.Count == 0)
                return null;
            List<int> innovations = ConnectionsByInnovation.Keys.ToList();
            int randomInnovation = Random.NextInt(0, innovations.Count);
            return ConnectionsByInnovation[randomInnovation];
        }

        public void MutateWeight()
        {
            ConnectionGene connection = GetRandomConnection();
            if (connection == null)
                return;
            connection.Weight = Random.NextFloat(-1, 1) * Config.WeightRandom;
        }

        public void MutateWeightShift()
        {
            ConnectionGene connection = GetRandomConnection();
            if (connection == null)
                return;
            connection.Weight += Random.NextFloat(-1, 1) * Config.WeightShift;
        }

        public void MutateIsEnable()
        {
            ConnectionGene connection = GetRandomConnection();
            if (connection == null)
                return;
            connection.IsEnabled = !connection.IsEnabled;
        }

        public NodeGene GetNodeById(int id)
        {
            return NodesByIds[id];
        }

        public void AddGene(Gene gene)
        {
            if (gene is NodeGene)
            {
                NodeGene node = gene as NodeGene;
                if (node.Pos.x <= 0 || 1 <= node.Pos.x)
                    AddHiddenNode(node);
                else
                    AddNode(node);
            }
            else
                AddConnection(gene as ConnectionGene);
        }

        public Gene GetGene(int innovation) 
        {
            if (NodesByInnovation.ContainsKey(innovation))
                return NodesByInnovation[innovation];
            else
                return ConnectionsByInnovation[innovation];
        }
    }
}
