﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ML
{
    public class Calculator
    {
        private Genome Genome;

        public Calculator(Genome genome)
        {
            Genome = genome;
        }

        public float[] Calculate(float[] inputs)
        {
            if (inputs.Length != Genome.Inputs.Count)
            {
                Debug.LogError("smt goes wrong!~~!");
                return null;
            }

            float[] outputs = new float[Genome.Outputs.Count];
            Genome.Hidden = Genome.Hidden.OrderBy(node => node.Pos.x).ToList();
            FillInputsWithInputData(inputs);
            FillHiddenNodesWithData();

            for (int i = 0; i < outputs.Length; i++)
            {
                float sum = 0;
                NodeGene node = Genome.Outputs[i];
                foreach (ConnectionGene connection in node.Connections)
                {
                    int fromId = connection.From;
                    NodeGene fromGene = Genome.GetNodeById(fromId);
                    sum += connection.Weight * fromGene.Output;
                }
                node.Output = ActivationFunction(sum);
                outputs[i] = node.Output;
            }           

            return outputs;
        }

        private void FillHiddenNodesWithData()
        {
            foreach (NodeGene node in Genome.Hidden)
            {
                float sum = 0;
                foreach (ConnectionGene connection in node.Connections)
                {
                    int fromId = connection.From;
                    NodeGene fromGene = Genome.GetNodeById(fromId);
                    sum += connection.Weight * fromGene.Output;
                }
                node.Output = ActivationFunction(sum);
            }
        }

        private void FillInputsWithInputData(float[] inputs)
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                Genome.Inputs[i].Output = inputs[i];
            }
        }

        private float ActivationFunction(float value)
        {
            return 1f / (1 + Mathf.Exp(-value));
        }
    }
}
