﻿using System;

namespace ML
{
    [Serializable]
    public class Config
    {
        public int MaxNodes = 1000000;
        public int Inputs = 2;
        public int Outputs = 2;

        public float C1 = 1;
        public float C2 = 1;
        public float C3 = 1;
        public float WeightShift = 0.3f;
        public float WeightRandom = 1;

        public float ProbAddLink = 0.1f;
        public float ProbAddNode = 0.05f;
        public float ProbWeightShift = 0.9f;
        public float ProbWeight = 0.45f;
        public float ProbIsEnable = 0.15f;

        public float CompabilityTreshold = 4;

        public float KillPercantage = 0.5f;

        public int Population = 100;
    }
}
