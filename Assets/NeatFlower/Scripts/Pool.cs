﻿using System.Collections.Generic;
using System.Linq;

namespace Neat
{
    public class Pool
    {
        private List<Spicies> Spicies;
        private int Generations;
        private float TopFitness;
        private int PoolStateleness;

        private SpiciesComparator Comparator;
        private Config Config;
        private IRandom Random;

        public Pool(IRandom random, Config config)
        {
            Spicies = new List<Spicies>();
            Comparator = new SpiciesComparator(config);
            Config = config;
            Random = random;
        }

        private void Init()
        {
            for (int i = 0; i < Config.Population; i++)
            {
                AddToSpecies(new Genome(Random, Config));
            }
        }

        private void AddToSpecies(Genome genome)
        {
            foreach (Spicies spicies in Spicies)
            {
                if (spicies.Genomes.Count == 0)
                    continue;

                Genome first = spicies.Genomes.First();
                if (Comparator.IsFromOneSpecies(first, genome))
                {
                    spicies.Genomes.Add(genome);
                    return;
                }
            }

            Spicies newSicies = new Spicies(Random, Config);
            newSicies.Genomes.Add(genome);
            Spicies.Add(newSicies);
        }
    }
}
