﻿using UnityEngine;

namespace Neat
{
    public class ConnectionGene 
    {
        public int IntoValue;
        public int OutValue;
        public int Innovation;
        public float Weight;
        public bool IsEnabled;

        public ConnectionGene(int intoValue, int outValue, int innovation, float weight, bool isEnabled)
        {
            IntoValue = intoValue;
            OutValue = outValue;
            Innovation = innovation;
            Weight = weight;
            IsEnabled = isEnabled;
        }

        public ConnectionGene(ConnectionGene connection)
        {
            IntoValue = connection.IntoValue;
            OutValue = connection.OutValue;
            Innovation = connection.Innovation;
            Weight = connection.Weight;
            IsEnabled = connection.IsEnabled;
        }

        public override string ToString()
        {
            return "in->" + IntoValue + " out->" + OutValue + " inn->" + Innovation + " w->" + Weight + " isE->" + IsEnabled;
        }
    } 
}
