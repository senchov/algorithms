﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Neat
{
    public class Mutation
    {
        private IRandom Random;
        private Config Config;

        public Mutation(IRandom random, Config config)
        {
            Random = random;
            Config = config;
        }

        public void Weight(List<ConnectionGene> connections)
        {
            foreach (ConnectionGene connection in connections)
            {
                if (Random.NextFloat() < Config.WeightChance)
                {
                    //if (Random.NextFloat() < Config.PerturbChance)
                    //    connection.Weight += Random.NextFloat(-1, 1) * Config.Steps;
                    //else
                    connection.Weight += Random.NextFloat(-2, 2);
                }
            }
        }

        public void AddConnection(List<ConnectionGene> connections, Dictionary<int, NodeGene> nodes)
        {
            int node1 = -1;
            int node2 = -1;

            GetNodes(nodes, ref node1, ref node2);

            if (node1 >= node2)
                return;
            // nodes[node2].Connections
            foreach (ConnectionGene gene in nodes[node2].Connections)
            {
                if (gene.IntoValue == node1)
                    return;
            }

            if (node1 < 0 || node2 < 0)
            {
                Debug.LogError("There is an issue");
                return;
            }
            int innovation = InnovationCounter.GetInnovation();
            float weight = Random.NextFloat(-2, 2);
            ConnectionGene newGene = new ConnectionGene(node1, node2, innovation, weight, true);
            connections.Add(newGene);
        }

        private void GetNodes(Dictionary<int, NodeGene> nodes, ref int node1, ref int node2)
        {
            int i = 0;
            int j = 0;
            int rand1 = Random.NextInt(0, nodes.Count());
            int rand2 = Random.NextInt(0, nodes.Count() - Config.Inputs - 1) + Config.Inputs + 1;

            foreach (int key in nodes.Keys)
            {
                if (rand1 == i)
                {
                    node1 = key;
                    break;
                }
                i += 1;
            }

            foreach (int key in nodes.Keys)
            {
                if (rand2 == j)
                {
                    node2 = key;
                    break;
                }
                j += 1;
            }
        }

        public void AddNode(List<ConnectionGene> connections, Dictionary<int, NodeGene> nodes)
        {
            if (connections.Count == 0)
                return;

            int count = 0;
            int random = Random.NextInt(0, connections.Count());
            ConnectionGene randGene = connections[random];
            while (!randGene.IsEnabled)
            {
                int nextRandom = Random.NextInt(0, connections.Count());
                randGene = connections[nextRandom];
                count += 1;
                if (count > Config.HiddenNodes)
                    return;
            }

            int nextNode = nodes.Count() - Config.Outputs;
            randGene.IsEnabled = false;
            int newInnovation = InnovationCounter.GetInnovation();
            connections.Add(new ConnectionGene(randGene.IntoValue, nextNode, newInnovation, 1, true));
            int newOldInnovation = InnovationCounter.GetInnovation();
            connections.Add(new ConnectionGene(nextNode, randGene.OutValue, newOldInnovation, randGene.Weight, true));
        }

        public void MutateDisable(List<ConnectionGene> connections) 
        {
            if (connections.Count() == 0)
                return;
            int random = Random.NextInt(0, connections.Count());
            connections[random].IsEnabled = false;
        }

        public void MutateEnable(List<ConnectionGene> connections)
        {
            if (connections.Count() == 0)
                return;
            int random = Random.NextInt(0, connections.Count());
            connections[random].IsEnabled = true;
        }
    }
}
