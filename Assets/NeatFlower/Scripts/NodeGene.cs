﻿using System.Collections.Generic;

namespace Neat
{
    public class NodeGene
    {
        public float Value;

        public List<ConnectionGene> Connections;

        public NodeGene(float value)
        {
            Value = value;
            Connections = new List<ConnectionGene>();
        }
    }
}
