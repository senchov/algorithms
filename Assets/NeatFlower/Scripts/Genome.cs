﻿using System.Collections.Generic;
using UnityEngine;

namespace Neat
{
    public class Genome
    {
        private IRandom Random;
        public float Fitness;
        private float Score;
        public List<ConnectionGene> Connections;
        private Dictionary<int, NodeGene> Nodes;
        public float AdjustedFitness;
        private Config Config;

        private CrossOver CrossOverInstance;
        private SpiciesComparator SpiciesComparator;
        private Mutation Mutation;

        public Genome(IRandom random, Config config)
        {
            Config = config;
            Random = random;
            CrossOverInstance = new CrossOver(random);
            SpiciesComparator = new SpiciesComparator(Config);
            Mutation = new Mutation(random, Config);
        }

        public Genome(Genome child)
        {
            foreach (ConnectionGene childConnection in child.Connections)
            {
                Connections.Add(childConnection);
            }

            Fitness = child.Fitness;
            AdjustedFitness = child.AdjustedFitness;
            Config = child.Config;
        }

        public Genome CrossOver(Genome first, Genome second)
        {
            return CrossOverInstance.Do(first, second);
        }

        public bool IsFromOneSpicies(Genome first, Genome second)
        {
            return SpiciesComparator.IsFromOneSpecies(first, second);
        }

        public void GenerateNetwork()
        {
            Nodes = new Dictionary<int, NodeGene>();
            FillNodesWithInputs();
            FillNodesWithHiddenAndOututs();

            foreach (ConnectionGene connection in Connections)
            {
                if (!Nodes.ContainsKey(connection.IntoValue))
                    Nodes.Add(connection.IntoValue, new NodeGene(0));

                if (!Nodes.ContainsKey(connection.OutValue))
                    Nodes.Add(connection.OutValue, new NodeGene(0));
                Nodes[connection.OutValue].Connections.Add(connection);
            }
        }

        private void FillNodesWithHiddenAndOututs()
        {
            int inputsHidden = Config.Inputs + Config.HiddenNodes;
            int inputsHiddenOutputs = inputsHidden + Config.Outputs;
            for (int i = inputsHidden; i < inputsHiddenOutputs; i++)
            {
                Nodes.Add(i, new NodeGene(0));
            }
        }

        private void FillNodesWithInputs()
        {
            for (int i = 0; i < Config.Inputs; i++)
            {
                Nodes.Add(i, new NodeGene(0));
            }
            Nodes.Add(Config.Inputs, new NodeGene(1));
        }

        public List<float> EvaluateNetwork(List<float> inputs)
        {
            List<float> outputs = new List<float>();
            for (int i = 0; i < inputs.Count; i++)
            {
                Nodes[i].Value = inputs[i];
            }

            SetNodesValue();

            for (int i = 0; i < Config.Outputs; i++)
            {
                int inputsHidden = Config.Inputs + Config.HiddenNodes;
                float value = Nodes[inputsHidden + i].Value;
                outputs.Add(value);
            }

            return outputs;
        }

        private void SetNodesValue()
        {
            foreach (KeyValuePair<int, NodeGene> nodeKVP in Nodes)
            {
                float sum = 0;
                int key = nodeKVP.Key;
                NodeGene node = nodeKVP.Value;
                if (Config.Inputs < key)
                {
                    foreach (ConnectionGene connection in node.Connections)
                    {
                        if (connection.IsEnabled)
                        {
                            sum += Nodes[connection.IntoValue].Value * connection.Weight;
                        }
                        node.Value = Sigmoid(sum);
                    }
                }
            }
        }

        private float Sigmoid(float x)
        {
            return (float)(1 / (1 + Mathf.Exp(-4.9f * x)));
        }

        public void Mutate()
        {
            if (Random.NextFloat() <= Config.WeightMutationChance)
                Mutation.Weight(Connections);

            if (Random.NextFloat() <= Config.ConnectionMutationChance)
            {
                GenerateNetwork();
                Mutation.AddConnection(Connections, Nodes);
            }

            if (Random.NextFloat() <= Config.NodeMutationChance)
            {
                GenerateNetwork();
                Mutation.AddNode(Connections, Nodes);
            }

            if (Random.NextFloat() <= Config.DisableMutationChance)
                Mutation.MutateDisable(Connections);

            if (Random.NextFloat() <= Config.EnableMutationChance)
                Mutation.MutateEnable(Connections);
        }
    }
}
