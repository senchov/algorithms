﻿namespace Neat
{
    public interface IRandom
    {
        float NextFloat();
        float NextFloat(float min, float max);
        int NextInt();
        int NextInt(int min, int max);
        bool NextBool();
    }
}
