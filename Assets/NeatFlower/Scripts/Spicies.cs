﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Neat
{
    public class Spicies
    {
        private IRandom Random;
        public float TopFitness;
        public int Stateless;
        public List<Genome> Genomes;
        private Config Config;
        private CrossOver CrossOver;

        public Spicies(IRandom random, Config config)
        {
            Random = random;
            Genomes = new List<Genome>();
            Config = config;
            CrossOver = new CrossOver(random);
        }

        public Spicies(Genome top, IRandom random, Config config)
        {
            Random = random;
            Genomes = new List<Genome>();
            Genomes.Add(top);
            Config = config;
            CrossOver = new CrossOver(random);
        }

        public void CalculateAdjustedFitness()
        {
            if (Genomes.Count == 0)
                return;
            foreach (Genome genome in Genomes)
            {
                genome.AdjustedFitness = genome.Fitness / Genomes.Count;
            }
        }

        public float GetTotalAdjsutedFitness()
        {
            return Genomes.Sum(x => x.AdjustedFitness);
        }
             
        public void RemoveWeakGenomes(bool allButOne)
        {
            Genomes.Reverse();
            int surviveCount = allButOne ? 1 : Mathf.CeilToInt(Genomes.Count / 2);
            Genomes = Genomes.Take(surviveCount).ToList();
        }

        public Genome GetTopGenome() 
        {
            return Genomes.Last();
        }

        public Genome BreedChild() 
        {
            Genome child;
            if (Random.NextFloat() < Config.CrossoverChance)
            {
                Genome first = Genomes[Random.NextInt(0, Genomes.Count)];
                Genome second = Genomes[Random.NextInt(0, Genomes.Count)];
                child = CrossOver.Do(first, second);
            }
            else 
            {
                child = Genomes[Random.NextInt(0, Genomes.Count)];
            }

            child = new Genome(child);
            child.Mutate();

            return child;
        }

        public float GetTopFitness() 
        {
            TopFitness = GetTopGenome().Fitness;
            return TopFitness;
        }
    }
}
