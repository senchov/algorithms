﻿using System.Collections.Generic;
using System.Linq;

namespace Neat
{
    public class CrossOver
    {
        private Genome FittestGenome;
        private Genome WeakerGenome;

        private Dictionary<int, ConnectionGene> FittestGenMap;
        private Dictionary<int, ConnectionGene> WeakerGenMap;

        private IRandom Random;

        public CrossOver(IRandom random)
        {
            Random = random;
        }

        public Genome Do(Genome genome_1, Genome genome_2)
        {
            IdentifyFittestAndWeaker(genome_1, genome_2);
            Genome child = new Genome(Random, null);
            FittestGenMap = FittestGenome.Connections.OrderBy(c => c.Innovation).ToDictionary(i => i.Innovation);
            WeakerGenMap = WeakerGenome.Connections.OrderBy(c => c.Innovation).ToDictionary(i => i.Innovation);

            List<int> existingInnovation = FittestGenMap.Keys.Concat(WeakerGenMap.Keys).Distinct().ToList();

            foreach (int innovation in existingInnovation)
            {
                ConnectionGene trait;
                if (HaveSameGene(innovation))
                {
                    if (Random.NextBool())
                        trait = FittestGenMap[innovation];
                    else
                        trait = WeakerGenMap[innovation];

                    if (FittestGenMap[innovation].IsEnabled != WeakerGenMap[innovation].IsEnabled)
                    {
                        trait.IsEnabled = Random.NextBool();
                    }
                }
                else
                {
                    if (HaveSameFitness())
                    {
                        if (FittestGenMap.ContainsKey(innovation))
                            trait = FittestGenMap[innovation];
                        else
                            trait = WeakerGenMap[innovation];
                    }
                    else
                    {
                        trait = FittestGenMap[innovation];
                    }
                }
                child.Connections.Add(trait);
            }

            return child;
        }

        private void IdentifyFittestAndWeaker(Genome genome_1, Genome genome_2)
        {
            if (genome_1.Fitness > genome_2.Fitness)
            {
                FittestGenome = genome_1;
                WeakerGenome = genome_2;
            }
            else
            {
                FittestGenome = genome_2;
                WeakerGenome = genome_1;
            }
        }

        private bool HaveSameFitness()
        {
            return FittestGenome.Fitness == WeakerGenome.Fitness;
        }

        private bool HaveSameGene(int innovation)
        {
            return FittestGenMap.ContainsKey(innovation) && WeakerGenMap.ContainsKey(innovation);
        }
    }
}
