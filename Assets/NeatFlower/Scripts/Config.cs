﻿namespace Neat
{
    public class Config
    {
        public int Inputs;
        public int Outputs;
        public int HiddenNodes;
        public int Population;

        public float CompabilityTreshold;
        public float ExecessCoef;
        public float DisjointCoef;
        public float WeightCoef;

        public float StaleSpicies;

        public float Steps;
        public float PerturbChance;
        public float WeightChance;
        public float WeightMutationChance;
        public float NodeMutationChance;
        public float ConnectionMutationChance;
        public float BiasConnectionMutationChance;
        public float DisableMutationChance;
        public float EnableMutationChance;
        public float CrossoverChance;

        public int StalePool;

        public Config()
        {
            Inputs = 2;
            Outputs = 1;
            HiddenNodes = 100000;
            Population = 300;

            CompabilityTreshold = 1;
            ExecessCoef = 2;
            DisjointCoef = 2;
            WeightCoef = 0.4f;

            StaleSpicies = 15;

            Steps = 0.1f;
            PerturbChance = 0.9f;
            WeightChance = 0.3f;
            WeightMutationChance = 0.9f;
            NodeMutationChance = 0.03f;
            ConnectionMutationChance = 0.05f;
            BiasConnectionMutationChance = 0.15f;
            DisableMutationChance = 0.1f;
            EnableMutationChance = 0.2f;
            CrossoverChance = 0.75f;

            StalePool = 20;
        }
    }
}
