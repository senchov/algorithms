﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Neat
{
    public class SpiciesComparator
    {
        private Dictionary<int, ConnectionGene> FirstGenMap;
        private Dictionary<int, ConnectionGene> SecondGenMap;

        private Config Config;

        public SpiciesComparator(Config config)
        {
            Config = config;
        }

        public bool IsFromOneSpecies(Genome first, Genome second)
        {
            FirstGenMap = first.Connections.OrderBy(c => c.Innovation).ToDictionary(i => i.Innovation);
            SecondGenMap = second.Connections.OrderBy(c => c.Innovation).ToDictionary(i => i.Innovation);

            int match = 0;
            int disjoint = 0;
            int excess = 0;
            float weight = 0;

            FillNumbers(out match, out disjoint, out excess, out weight);

            int n = match + disjoint + excess;

            float delta = n > 0 ? GetDelta(match, disjoint, excess, weight, n) : 0;

            return delta < Config.CompabilityTreshold;
        }

        private void FillNumbers(out int match, out int disjoint, out int excess, out float weight)
        {
            List<int> existingInnovation = FirstGenMap.Keys.Concat(SecondGenMap.Keys).Distinct().ToList();
            match = 0;
            disjoint = 0;
            excess = 0;
            weight = 0;

            int lowestInnovation = GetLowestInnovation();

            foreach (int innovation in existingInnovation)
            {
                if (IsMatch(innovation))
                {
                    match += 1;
                    weight += Mathf.Abs(FirstGenMap[innovation].Weight - SecondGenMap[innovation].Weight);
                }
                else
                {
                    if (innovation < lowestInnovation)
                        disjoint += 1;
                    else
                        excess += 1;
                }
            }
        }

        private float GetDelta(int match, int disjoint, int excess, float weight, int n)
        {
            float excessCoef = Config.ExecessCoef;
            float disjointCoef = Config.DisjointCoef;
            float weightCoef = Config.WeightCoef;

            return (excessCoef * excess + disjointCoef * disjoint) / n + weightCoef * weight / n;
        }

        private int GetLowestInnovation()
        {
            int lowMaxInnovation;
            if (FirstGenMap.Count == 0 || SecondGenMap.Count == 0)
                lowMaxInnovation = 0;
            else
                lowMaxInnovation = Mathf.Min(FirstGenMap.Last().Key, SecondGenMap.Last().Key);
            return lowMaxInnovation;
        }

        private bool IsMatch(int innovation)
        {
            return FirstGenMap.ContainsKey(innovation) && SecondGenMap.ContainsKey(innovation);
        }       
    }
}
