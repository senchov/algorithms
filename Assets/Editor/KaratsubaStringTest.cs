﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KaratsubaStringTest
{
    int AdbcCounter;

    [Test]
    public void Test()
    {
        int n = 0;
        n = GetN(0);
        Assert.AreEqual(1, n);
        Assert.AreEqual(2, GetN(1));
        Assert.IsTrue(IsPowerOfTwo(2));
        Assert.AreEqual(4, GetN(3));
        Assert.IsTrue(IsPowerOfTwo(4));
        Assert.AreEqual(8, GetN(5));
        Assert.AreEqual(8, GetN(7));
        Assert.IsTrue(IsPowerOfTwo(8));

        Assert.AreEqual("1", Mul("1", "1"));
        Assert.AreEqual("2", Mul("1", "2"));
        Assert.AreEqual("4", Mul("2", "2"));
        Assert.AreEqual("144", Mul("12", "12"));
        Assert.AreEqual((13*13).ToString(), Mul("13", "13"));
        Assert.AreEqual((253 * 366).ToString(), Mul("253", "366"));
        Assert.AreEqual((1234 * 5678).ToString(), Mul("1234", "5678"));

        AdbcCounter = 0;
        Assert.AreEqual("2205647016448403", Mul("49823261", "44269423", "7"));
        Debug.Log(AdbcCounter);
        Assert.AreEqual("3581108403425012", Mul("54761293", "65394884"));
        Assert.AreEqual("71332574014261268360454523927286", Mul("9313685456934674", "7658898761837539"));

        AdbcCounter = 0;
        Assert.AreEqual("25478534007255378799894857247961445544397925869179138904636157575535921570058983065006369481295619500406669960288667484926076424",
            Mul("8711129198194917883527844183686122989894424943636426448417394566",
            "2924825637132661199799711722273977411715641477832758942277358764", "58"));
        Assert.AreEqual(12, AdbcCounter);

        AdbcCounter = 0;
        string res = Mul("1685287499328328297814655639278583667919355849391453456921116729", "7114192848577754587969744626558571536728983167954552999895348492", "12");
        Debug.Log(res + "   " + AdbcCounter);
    }

    private string Mul(string x, string y, string findNum = "0")
    {
        int n = Mathf.Max(x.Length, y.Length);

        if (n <= 1)
            return RealMul(x, y);

        if (!IsPowerOfTwo(n))
            n = GetN(n);
        x = AlignNumber(x, n);
        y = AlignNumber(y, n);

        string a = GetFirstNum(x);
        string b = GetSecondNum(x);
        string c = GetFirstNum(y);
        string d = GetSecondNum(y);

        string ac = Mul(a, c, findNum);
        string bd = Mul(b, d, findNum);

        StringAdder adder = new StringAdder();
        string aPLUSb = adder.Plus(a, b);
        string cPLUSd = adder.Plus(c, d);

        string abcd = Mul(aPLUSb, cPLUSd, findNum);

        string adPLUSbc = adder.Minus(abcd, ac, bd);
        if (adPLUSbc == findNum)
            AdbcCounter++;

        string acPowN = PowOfTen(ac, n);
        string adPLUSbcPOWnDIV2 = PowOfTen(adPLUSbc, n / 2);

        return adder.Plus(acPowN, adPLUSbcPOWnDIV2, bd);
    }

    private string PowOfTen(string num, int pow)
    {
        for (int i = 0; i < pow; i++)
        {
            num += "0";
        }
        return num;
    }

    private string RealMul(string first, string second)
    {
        int f = int.Parse(first);
        int s = int.Parse(second);
        int mul = s * f;
        return mul.ToString();
    }

    private string GetFirstNum(string num)
    {
        int half = num.Length / 2;
        string substring = num.Substring(0, half);
        return substring;
    }

    private string GetSecondNum(string num)
    {
        int half = num.Length / 2;
        string substring = num.Substring(half);
        return substring;
    }

    private bool IsPowerOfTwo(int num)
    {
        return (num != 0) && (num & (num - 1)) == 0;
    }

    private int GetN(int num)
    {
        int pow = 0;
        while (num > 0)
        {
            num >>= 1;
            pow++;
        }
        return 1 << pow;
    }

    private string AlignNumber(string num, int n)
    {
        while (num.Length < n)
        {
            num = "0" + num;
        }
        return num;
    }
}

//X: 54761293
//Y: 65394884
//Z: 3581108403425012
//Value counter
//5 -> [4, 2, 2]

//X: 9313685456934674
//Y: 7658898761837539
//Z: 71332574014261268360454523927286
//Value counter
//18 -> [5, 1, 2]
//9 -> [7, 3, 3]
//8 -> [8, 7, 5]

//X: 3957322621234423
//Y: 7748313756335578
//Z: 30662577304368647842211393201494
//Value counter
//14 -> [5, 1, 1]
//9 -> [6, 2, 5]
//8 -> [13, 8, 7]

//X: 34215432964249374812219364786397
//Y: 94541964835273822784327848699719
//Z: 3234794260129733170788831535430575611379062580407060392628922443
//Value counter
//45 -> [11, 3, 0]
//9 -> [28, 19, 14]
//8 -> [17, 11, 10]

//X: 71611955325935479159397713213124
//Y: 93567726499788166681348352945366
//Z: 6700567850052179472481148730882040129649508491917840721086183384
//Value counter
//40 -> [15, 4, 2]
//36 -> [11, 2, 2]
//10 -> [11, 4, 6]

//X: 8436939677358274975644341226884162349535836199962392872868456892
//Y: 3864264464372346883776335161325428226997417338413342945574123327
//Z: 32602566183268675582196165592691544162522778809155901895617284287276672563976841699892789718741377833554298336397153444191119684
//Value counter
//72 -> [14, 4, 5]
//69 -> [12, 3, 0]
//67 -> [14, 1, 3]

//X: 8711129198194917883527844183686122989894424943636426448417394566
//Y: 2924825637132661199799711722273977411715641477832758942277358764
//Z: 25478534007255378799894857247961445544397925869179138904636157575535921570058983065006369481295619500406669960288667484926076424
//Value counter
//64 -> [20, 5, 3]
//60 -> [12, 3, 4]
//58 -> [12, 5, 2]

