﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

public class LongestSubstringWithoutRepeatingCharacters
{
    [Test]
    public void Start()
    {
        int six = LengthOfLongestSubstring("dvdf");
        Assert.AreEqual(3, six);
        int fifth = LengthOfLongestSubstring("aab");
        Assert.AreEqual(2, fifth);
        int first = LengthOfLongestSubstring("abcabcbb");
        Assert.AreEqual(3, first);
        int second = LengthOfLongestSubstring("bbbbb");
        Assert.AreEqual(1, second);
        int third = LengthOfLongestSubstring(" ");
        Assert.AreEqual(1, third);
        int fourth = LengthOfLongestSubstring("au");
        Assert.AreEqual(2, fourth);
    }

    public int LengthOfLongestSubstring(string s)
    {
        char[] arr = s.ToCharArray();
        int head = 0;
        int tail = 1;
        int maxLength = s == String.Empty ? 0 : 1;
        List<char> inSubstring = new List<char>();

        while (head < arr.Length)
        {
            char currrent = arr[head];

            if (!inSubstring.Contains(currrent))
            {
                inSubstring.Add(currrent);
                head++;
            }
            else
            {
                maxLength = Math.Max(maxLength, inSubstring.Count);
                inSubstring = new List<char>();               
                head = tail;
                tail++;
            }
            maxLength = Math.Max(maxLength, inSubstring.Count);
        }
        return maxLength;
    }
}
