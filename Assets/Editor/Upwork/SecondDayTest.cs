﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace CodeSignal
{
    public class SecondDayTest
    {
        [Test]
        public void Test()
        {
        }

        private void TestLongest()
        {
            int[] a = new int[] { 2, 1, 7, 1, 1, 5, 3, 5, 2, 1, 1, 1 };
            int[] b = new int[] { 1, 3, 5 };
            int[] c = new int[] { 2, 3 };
            int[] arr = GetArr(a, b, c);
            Assert.AreEqual(3, arr.Length);
            Assert.AreEqual(1, arr[0]);
            Assert.AreEqual(1, arr[1]);
            Assert.AreEqual(5, arr[2]);
        }

        private int[] GetArr(int[] a, int[] b, int[] c)
        {
            List<int> longest = new List<int>();
            List<int> current = new List<int>();

            for (int i = 0; i < a.Length; i++)
            {
                int e = a[i];
                if (IsAcceptable(e, b, c))
                    current.Add(e);
                else
                {
                    if (current.Count > longest.Count) 
                    {
                        longest = current;
                    }
                    current = new List<int>();
                }
            }

            return longest.ToArray();
        }

        private bool IsAcceptable(int element, int[] b, int[] c)
        {
            return b.Contains(element) && !c.Contains(element); ;
        }

        

        private void DiagonalMartix()
        {
            //m:
            //[[1,4,-2], 
            //[-2,3,4], 
            //[3,1,3]]
            int[][] preferences = new int[][]
              {
                new int[]{ 1,4,-2 },
                new int[]{ -2,3,4},
                new int[]{ 3,1,3 },
              };
            DisplayMatrix(preferences);
            sortMatrixByOccurrences(preferences);
            DisplayMatrix(preferences);

            int[][] op = new int[][]
              {
                new int[]{ 5,4},
                new int[]{ 4,5}
              };
            sortMatrixByOccurrences(op);
            DisplayMatrix(op);

            int[][] op1 = new int[][]
              {
                new int[]{ -5,3,9,8},
                new int[]{ 2,-7,-2,10},
                new int[]{ -8,-4,9,-1},
                new int[]{ -6,-4,9,-3},
              };
            DisplayMatrix(op1);
            sortMatrixByOccurrences(op1);
            DisplayMatrix(op1);
        }

        private static void DisplayMatrix(int[][] preferences)
        {
            string rowString = "";
            for (int i = 0; i < preferences.Length; i++)
            {
                int[] row = preferences[i];
                rowString += row.Select(x => x.ToString()).Aggregate("", (concat, element) => $"{concat},{element}") + "\n";
            }
            Debug.Log(rowString);
        }

        private int[][] sortMatrixByOccurrences(int[][] m)
        {
            Dictionary<int, int> countByNumber = new Dictionary<int, int>();
            FillDict(m, countByNumber);
            countByNumber = countByNumber.OrderByDescending(kvp => kvp.Value).ThenByDescending(kvp => kvp.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            Queue<int> queue = new Queue<int>();
            foreach (var item in countByNumber)
            {
                for (int i = 0; i < item.Value; i++)
                {
                    queue.Enqueue(item.Key);
                }
            }

            Dictionary<int, List<(int, int)>> diagonals = new Dictionary<int, List<(int, int)>>();
            for (int i = 0; i < m.Length; i++)
            {
                for (int j = 0; j < m[i].Length; j++)
                {
                    int index = i + j;
                    if (diagonals.ContainsKey(index))
                        diagonals[index].Add((i, j));
                    else
                        diagonals[index] = new List<(int i, int j)>() { (i, j) };
                }
            }
            diagonals = diagonals.OrderBy(kvp => kvp.Key).ToDictionary(d => d.Key, d => d.Value);

            foreach (var item in diagonals)
            {
                foreach (var iandj in item.Value)
                {
                    int i = iandj.Item1;
                    int j = iandj.Item2;
                    int num = queue.Dequeue();
                    m[i][j] = num;
                }
            }

            return m;
        }

        private class Holder
        {
            public int I;

        }

        private void Fill(int[][] m, int i, int j, Queue<int> queue, bool toRight)
        {
            if (i >= m.Length)
                return;
            if (j >= m.Length)
                return;
            int num = queue.Dequeue();
            m[i][j] = num;
            Debug.Log($"i->{i} j->{j}");
            if (toRight)
                Fill(m, i, j + 1, queue, false);
            else
                Fill(m, i, j, queue, true);
        }

        private void FillDict(int[][] m, Dictionary<int, int> countByNumber)
        {
            for (int i = 0; i < m.Length; i++)
            {
                for (int j = 0; j < m[i].Length; j++)
                {
                    int num = m[i][j];
                    if (countByNumber.ContainsKey(num))
                        countByNumber[num] += 1;
                    else
                        countByNumber.Add(num, 1);
                }
            }
        }

        private void TinyTest()
        {
            int[] a = new int[] { 16, 1, 4, 2, 14 };
            int[] b = new int[] { 7, 11, 2, 0, 15 };
            int k = 743;
            int count = countTinyPairs(a, b, k);
            Assert.AreEqual(4, count);
        }

        int countTinyPairs(int[] a, int[] b, int k)
        {
            int count = 0;
            int i = 0;
            int j = b.Length - 1;
            while (i <= a.Length && j >= 0)
            {
                string concat = $"{a[i]}{b[j]}";
                int realK = int.Parse(concat);
                Debug.Log(concat);
                if (realK < k)
                    count += 1;
                i++;
                j--;
            }

            return count;
        }

        private int[] isZigzag(int[] numbers)
        {
            List<int> arr = new List<int>();
            for (int i = 1; i < numbers.Length - 1; i++)
            {
                int a = numbers[i - 1];
                int b = numbers[i];
                int c = numbers[i + 1];
                if (IsZigZag(a, b, c))
                    arr.Add(1);
                else
                    arr.Add(0);
            }

            return arr.ToArray();
        }

        private bool IsZigZag(int a, int b, int c)
        {
            bool first = a < b && b > c;
            bool second = a > b && b < c;
            return first || second;
        }
    }
}