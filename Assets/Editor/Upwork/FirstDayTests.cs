﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace CodeSignal
{
    public class FirstDayTests
    {
        [Test]
        public void Test()
        {
            int[][] preferences = new int[][]
            {
            new int[]{ 1,2,3,4 },
            new int[]{ 3,1,4,2 },
            new int[]{ 4,2,1,3 },
            };
            int r = preferredRestaurant(preferences);
            Assert.AreEqual(1, r);

            preferences = new int[][]
            {
            new int[]{ 1,2,3 },
            new int[]{ 2,3,1 },
            new int[]{ 3,1,2 },
            };
            r = preferredRestaurant(preferences);
            Assert.AreEqual(-1, r);
        }

        private int preferredRestaurant(int[][] matrix)
        {
            Dictionary<int, int> preferebalityByR = new Dictionary<int, int>();
            int[] firstRow = matrix[0];

            for (int i = 0; i < firstRow.Length; i++)
            {
                int r = firstRow[i];
                int preferability = 0;
                for (int j = 0; j < matrix.Length; j++)
                {
                    int[] row = matrix[j];
                    int indexOfR = Array.IndexOf(row, r);
                    preferability += indexOfR;
                }
                preferebalityByR.Add(r, preferability);
            }

            foreach (var item in preferebalityByR)
            {
                Debug.Log($"{item.Key} {item.Value}");
            }

            preferebalityByR = preferebalityByR.OrderBy(x => x.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            foreach (var item in preferebalityByR)
            {
                Debug.Log($"order {item.Key} {item.Value}");
            }
            List<int> keys = preferebalityByR.Keys.ToList();

            int first = preferebalityByR[keys[0]];
            int second = preferebalityByR[keys[1]];
            Debug.Log($"first->{first} + second->{second}");
            if (first < second)
                return keys[0];
            return -1;
        }

        private void CountWaysToSplitTest()
        {
            int count = countWaysToSplit("xzxzx");
            Assert.AreEqual(5, count);
        }

        private int countWaysToSplit(string s)
        {
            int count = 0;
            List<string> arr = s.ToCharArray().Select(x => x.ToString()).ToList();
            int lastIndex = arr.Count - 1;
            for (int i = 1; i < arr.Count - 1; i++)
            {
                for (int j = i + 1; j < arr.Count; j++)
                {
                    string a = arr.Take(i).Aggregate(string.Empty, (concat, str) => $"{concat}{str}");
                    int diff = j - i;
                    string b = arr.Skip(i).Take(diff).Aggregate(string.Empty, (concat, str) => $"{concat}{str}");

                    string c = arr.Skip(j).Aggregate(string.Empty, (concat, str) => $"{concat}{str}");

                    string ab = a + b;
                    string bc = b + c;
                    string ca = c + a;

                    bool ab_bc = ab != bc;
                    bool bc_ca = bc != ca;
                    bool ab_ca = ab != ca;

                    if (ab_bc && bc_ca && ab_ca)
                        count += 1;
                }
            }
            return count;
        }

        private void ReadingVerticallyTest()
        {
            string[] arr = new string[4] { "Daisy", "Rose", "Hyacinth", "Poppy" };
            readingVertically(arr);
        }

        string readingVertically(string[] arr)
        {
            string result = "";
            int arrayLength = arr.Length;

            List<List<string>> listOfLists = new List<List<string>>();
            for (int i = 0; i < arr.Length; i++)
            {
                char[] charArr = arr[i].ToCharArray();
                listOfLists.Add(new List<string>(charArr.Select(c => c.ToString())));
            }

            int maxLength = arr.Max(x => x.Length);
            for (int i = 0; i < maxLength; i++)
            {
                for (int k = 0; k < listOfLists.Count; k++)
                {
                    List<string> current = listOfLists[k];
                    if (i < current.Count)
                    {
                        result += current[i];
                    }
                }
            }

            return result;
        }
    } 
}
