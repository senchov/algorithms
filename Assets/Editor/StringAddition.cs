﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class StringAddition
{
    [Test]
    public void Test()
    {
        string result = "";
        StringAdder adder = new StringAdder();

        result = adder.Plus("1", "1");
        Assert.AreEqual("2", result);

        result = adder.Plus("10", "15");
        Assert.AreEqual("25", result);

        result = adder.Plus("65", "3");
        Assert.AreEqual("68", result);
        
        result = adder.Plus("1244", "666");
        Assert.AreEqual((1244 + 666).ToString(), result);

        result = adder.Plus("999955", "5893");
        Assert.AreEqual((999955 + 5893).ToString(), result);

        result = adder.Plus("85", "7777777");
        Assert.AreEqual((85 + 7777777).ToString(), result);

        result = adder.Minus("2", "1");
        Assert.AreEqual((2 - 1).ToString(), result);

        result = adder.Minus("25", "3");
        Assert.AreEqual((25 - 3).ToString(), result);

        result = adder.Minus("66688", "356");
        Assert.AreEqual((66688 - 356).ToString(), result);

        result = adder.Minus("66688", "66688");
        Assert.AreEqual((66688 - 66688).ToString(), result);

        result = adder.Minus("5899966666", "253664");
        Assert.AreEqual("5899713002", result);
    }    
}

