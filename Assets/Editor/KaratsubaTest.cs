﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KaratsubaTest
{
    [Test]
    public void Test()
    {
        int result = 0;
        result = Mul(1, 2, 1);
        Assert.AreEqual(2, result);

        result = Mul(2, 2, 0);
        Assert.AreEqual(4, result);

        result = Mul(5, 5, 1);
        Assert.AreEqual(25, result);

        result = Mul(12, 34, 2);
        Assert.AreEqual(408, result);

        //result = Mul(123, 456, 3);
        //Assert.AreEqual(56088, result);

        result = Mul(1234, 5678, 4);
        Assert.AreEqual(7006652, result);
    }

    private int Mul(int x, int y, int n = 0)
    {
        int a = GetFirstNum(x);
        int b = GetSecondNum(x);
        int c = GetFirstNum(y);
        int d = GetSecondNum(y);

        int ac = RecursionOrMul(n, a, c);
        int bd = RecursionOrMul(n, b, d);

        int aPLUSb = a + b;
        int cPLUSd = c + d;

        int abcd = RecursionOrMul(n, aPLUSb, cPLUSd);

        int adPLUSbc = abcd - ac - bd;      

        int tenN = PowOfTen(n);
        int tenNdivide2 = PowOfTen(n / 2);
        return tenN * ac + tenNdivide2 * adPLUSbc + bd;
    }

    private int GetFirstNum(int x)
    {
        if (x > 10)
            return x / 10;
        return 0;
    }

    private int GetSecondNum(int x)
    {
        return x % 10;
    }

    private int RecursionOrMul(int n, int a, int c)
    {
        int ac;
        if (a >= 10 || c >= 10)
        {
            ac = Mul(a, c, n / 2);
        }
        else
            ac = a * c;
        return ac;
    }

    private int PowOfTen(int n)
    {
        int pow = 1;
        for (int i = 0; i < n; i++)
        {
            pow *= 10;
        }
        return pow;
    }

    private int GetN(int number)
    {
        int n = 0;
        while (number > 0)
        {
            number /= 10;
            n++;
        }
        return n;
    }
}
